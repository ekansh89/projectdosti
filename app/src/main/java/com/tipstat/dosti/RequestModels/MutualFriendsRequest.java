package com.tipstat.dosti.RequestModels;

/**
 * Created by a on 10/25/2016.
 */

public class MutualFriendsRequest {

    private String fb_id;

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }
}
