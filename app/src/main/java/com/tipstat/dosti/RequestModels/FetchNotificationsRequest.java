package com.tipstat.dosti.RequestModels;

/**
 * Created by nikhi on 10/24/2016.
 */

public class FetchNotificationsRequest {

    private String fb_id;

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }
}
