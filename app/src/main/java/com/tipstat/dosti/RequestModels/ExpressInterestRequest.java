package com.tipstat.dosti.RequestModels;

/**
 * Created by nikhi on 10/24/2016.
 */

public class ExpressInterestRequest {

    private String fb_id1,fb_id2;

    public String getFb_id1() {
        return fb_id1;
    }

    public void setFb_id1(String fb_id1) {
        this.fb_id1 = fb_id1;
    }

    public String getFb_id2() {
        return fb_id2;
    }

    public void setFb_id2(String fb_id2) {
        this.fb_id2 = fb_id2;
    }
}
