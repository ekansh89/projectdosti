package com.tipstat.dosti.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Picasso;
import com.tipstat.dosti.Models.ChipTag;
import com.tipstat.dosti.Models.LocationModel;
import com.tipstat.dosti.Models.LoginModel;
import com.tipstat.dosti.Models.MainActivityAlertFragmentModel;
import com.tipstat.dosti.Models.MainActivityFriendsListModel;
import com.tipstat.dosti.Models.MutualLikesObject;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.ExpressInterestRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.ChipView.ChipsView;
import com.tipstat.dosti.Views.ChipView.Contact;
import com.tipstat.dosti.Views.CustomProgressDialog;
import com.tipstat.dosti.Views.FaceDetector.FaceCrop;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


public class UserProfile_SomeActionActivity extends AppCompatActivity {


    private ImageView profilepicture;
    private TextView location, job, company, gender, languages, religion, relationship, hometown, aboutme, languagetitle, title;
    private ProgressBar progressBar;
    private ScrollView scrollView;
    private View view, view1;
    private RelativeLayout religionlayout, relationshiplayout, hometownlayout;
    private TextView mutuallikestitle, liketitle;
    private Button acceptrequest;
    private TextView action;
    private RelativeLayout acceptRequestLayout;

    private CustomProgressDialog progressDialog;
    private MainActivityAlertFragmentModel mainActivityAlertFragmentModel;
    private Boolean actionPerformed = false;
    private ImageView back;
    private ChipView chip, mutuallikesChip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile__some_action);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        // getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(" ");
        // getSupportActionBar().setIcon(R.mipmap.alvoff_logo_wb);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });



        acceptrequest = (Button) findViewById(R.id.acceptrequest);
        profilepicture = (ImageView) findViewById(R.id.thumbnail);
        location = (TextView) findViewById(R.id.location);
        job = (TextView) findViewById(R.id.designation);
        company = (TextView) findViewById(R.id.organisation);
        title = (TextView) findViewById(R.id.title);
        gender = (TextView) findViewById(R.id.gender);
        action = (TextView) findViewById(R.id.action);
        aboutme = (TextView) findViewById(R.id.aboutme);
        hometown = (TextView) findViewById(R.id.hometown);
        acceptRequestLayout = (RelativeLayout) findViewById(R.id.acceptrequestlayout);
        relationship = (TextView) findViewById(R.id.relationship);
        languagetitle = (TextView) findViewById(R.id.languagetitle);
        this.view = findViewById(R.id.view);
        this.view1 = findViewById(R.id.view1);
        religion = (TextView) findViewById(R.id.religion);
        languages = (TextView) findViewById(R.id.language);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        relationshiplayout = (RelativeLayout) findViewById(R.id.relationshiplayout);
        religionlayout = (RelativeLayout) findViewById(R.id.religionlayout);
        hometownlayout = (RelativeLayout) findViewById(R.id.hometownlayout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        mutuallikestitle = (TextView) findViewById(R.id.mutualikestitle);
        liketitle = (TextView) findViewById(R.id.likestitle);
        back = (ImageView) findViewById(R.id.back);
        chip = (ChipView) findViewById(R.id.text_chip_attrs);
        mutuallikesChip = (ChipView) findViewById(R.id.text_chip_attrs_mutuallikes);


        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_white_24px);
        upArrow.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        back.setImageDrawable(upArrow);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("userobject")) {
                mainActivityAlertFragmentModel = (MainActivityAlertFragmentModel) intent.getSerializableExtra("userobject");
                getProfileDetails(mainActivityAlertFragmentModel.getFb_id2());
                action.setText(returnStatus(mainActivityAlertFragmentModel.getFlag(), mainActivityAlertFragmentModel.getName()));
                if (mainActivityAlertFragmentModel.getFlag().equalsIgnoreCase("3")) {
                    acceptRequestLayout.setVisibility(View.VISIBLE);
                    acceptRequestLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    acceptrequest.setTextColor(getResources().getColor(android.R.color.white));
                    acceptrequest.setText("  Facebook");

                    /*Drawable image = getResources().getDrawable(R.mipmap.ic_facebook);
                    int h = image.getIntrinsicHeight()-20;
                    int w = image.getIntrinsicWidth();
                    image.setBounds(0, 0, w, h);

                    acceptrequest.setCompoundDrawables(image,null,null,null);*/

                    acceptrequest.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.facebook), null, null, null);
                    acceptrequest.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(UserProfile_SomeActionActivity.this, webview.class);
                            intent.putExtra("url", "http://www.facebook.com/" + mainActivityAlertFragmentModel.getFb_id2());
                            startActivity(intent);

                        }
                    });

                    acceptRequestLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(UserProfile_SomeActionActivity.this, webview.class);
                            intent.putExtra("url", "http://www.facebook.com/" + mainActivityAlertFragmentModel.getFb_id2());
                            startActivity(intent);
                        }
                    });
                } else if (mainActivityAlertFragmentModel.getFlag().equalsIgnoreCase("4")) {

                    acceptRequestLayout.setVisibility(View.VISIBLE);
                    acceptRequestLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    acceptrequest.setTextColor(getResources().getColor(android.R.color.white));
                    acceptrequest.setText("  Share Friendship on Facebook");
                    acceptrequest.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.facebook), null, null, null);

                    acceptrequest.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final ShareDialog shareDialog = new ShareDialog(UserProfile_SomeActionActivity.this);
                            if (ShareDialog.canShow(ShareLinkContent.class)) {

                                ShareLinkContent content = new ShareLinkContent.Builder()
                                        .setContentUrl(Uri.parse("http://www.projectdosti.com/"))
                                        .setContentTitle("Project Dosti")
                                        .setImageUrl(Uri.parse("http://www.projectdosti.com/img/logo.png"))
                                        .setContentDescription("I met " + mainActivityAlertFragmentModel.getName() + " through Project Dosti")
                                        .build();
                                shareDialog.show(content);

                            } else {
                                Intent intent = new Intent(UserProfile_SomeActionActivity.this, webview.class);
                                intent.putExtra("url", "http://www.facebook.com/" + mainActivityAlertFragmentModel.getFb_id2());
                                startActivity(intent);
                            }
                        }
                    });
                    acceptRequestLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final ShareDialog shareDialog = new ShareDialog(UserProfile_SomeActionActivity.this);
                            if (ShareDialog.canShow(ShareLinkContent.class)) {

                                ShareLinkContent content = new ShareLinkContent.Builder()
                                        .setContentUrl(Uri.parse("http://www.projectdosti.com/"))
                                        .setContentTitle("Project Dosti")
                                        .setImageUrl(Uri.parse("http://www.projectdosti.com/img/logo.png"))
                                        .setContentDescription("I met " + mainActivityAlertFragmentModel.getName() + " through Project Dosti")
                                        .build();
                                shareDialog.show(content);

                            } else {
                                Intent intent = new Intent(UserProfile_SomeActionActivity.this, webview.class);
                                intent.putExtra("url", "http://www.facebook.com/" + mainActivityAlertFragmentModel.getFb_id2());
                                startActivity(intent);
                            }
                        }
                    });


                } else if (mainActivityAlertFragmentModel.getFlag().equalsIgnoreCase("1")) {
                    acceptRequestLayout.setVisibility(View.GONE);
                } else {
                    acceptrequest.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            acceptRequestLayout.setVisibility(View.VISIBLE);
                            progressDialog = CustomProgressDialog.show(UserProfile_SomeActionActivity.this);
                            acceptRequest(mainActivityAlertFragmentModel.getFb_id2());
                        }
                    });

                    acceptRequestLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            acceptRequestLayout.setVisibility(View.VISIBLE);
                            progressDialog = CustomProgressDialog.show(UserProfile_SomeActionActivity.this);
                            acceptRequest(mainActivityAlertFragmentModel.getFb_id2());
                        }
                    });
                }
            } else if (intent.hasExtra("friendobject"))

            {
                final MainActivityFriendsListModel mainActivityFriendsListModel = (MainActivityFriendsListModel) intent.getSerializableExtra("friendobject");
                getProfileDetails(mainActivityFriendsListModel.getFbId());
                action.setText(returnStatus("4", mainActivityFriendsListModel.getName()));

                acceptRequestLayout.setVisibility(View.VISIBLE);
                acceptRequestLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                acceptrequest.setTextColor(getResources().getColor(android.R.color.white));
                acceptrequest.setText("  Share Friendship on Facebook");

                acceptrequest.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.facebook), null, null, null);
                acceptrequest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       /* Intent intent = new Intent(UserProfile_SomeActionActivity.this, webview.class);
                        intent.putExtra("url", "http://www.facebook.com/" + mainActivityFriendsListModel.getFbId());
                        startActivity(intent);*/

                        final ShareDialog shareDialog = new ShareDialog(UserProfile_SomeActionActivity.this);
                        if (ShareDialog.canShow(ShareLinkContent.class)) {

                            ShareLinkContent content = new ShareLinkContent.Builder()
                                    .setContentUrl(Uri.parse("http://www.projectdosti.com/"))
                                    .setContentTitle("Project Dosti")
                                    .setImageUrl(Uri.parse("http://www.projectdosti.com/img/logo.png"))
                                    .setContentDescription("I met " + mainActivityFriendsListModel.getName() + " through Project Dosti")
                                    .build();
                            shareDialog.show(content);

                        } else {
                            Intent intent = new Intent(UserProfile_SomeActionActivity.this, webview.class);
                            intent.putExtra("url", "http://www.facebook.com/" + mainActivityAlertFragmentModel.getFb_id2());
                            startActivity(intent);
                        }


                    }
                });

                acceptRequestLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final ShareDialog shareDialog = new ShareDialog(UserProfile_SomeActionActivity.this);
                        if (ShareDialog.canShow(ShareLinkContent.class)) {

                            ShareLinkContent content = new ShareLinkContent.Builder()
                                    .setContentUrl(Uri.parse("http://www.projectdosti.com/"))
                                    .setContentTitle("Project Dosti")
                                    .setImageUrl(Uri.parse("http://www.projectdosti.com/img/logo.png"))
                                    .setContentDescription("I met " + mainActivityFriendsListModel.getName() + " through Project Dosti")
                                    .build();
                            shareDialog.show(content);

                        } else {
                            Intent intent = new Intent(UserProfile_SomeActionActivity.this, webview.class);
                            intent.putExtra("url", "http://www.facebook.com/" + mainActivityAlertFragmentModel.getFb_id2());
                            startActivity(intent);
                        }
                    }
                });

            }

        }

    }

    public int getAge(int _year, int _month, int _day) {

        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, a;

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);
        a = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH))
                || ((m == cal.get(Calendar.MONTH)) && (d < cal
                .get(Calendar.DAY_OF_MONTH)))) {
            --a;
        }
        if (a < 0)
            throw new IllegalArgumentException("Age < 0");
        return a;
    }
    public static Intent getOpenFacebookIntent(Context context, String fbid) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://profile/" + fbid)); //Trys to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/arkverse")); //catches and opens a url to the desired page
        }
    }


    public String returnStatus(String flag, String username) {
        if (flag != null) {
            if (flag.equalsIgnoreCase("2")) {
                return username + " has expressed interest";
            } else if (flag.equalsIgnoreCase("1")) {
                return "You have expressed interest on " + username;
            } else if (flag.equalsIgnoreCase("3"))
                return username + " and you are matched";
            else if (flag.equalsIgnoreCase("4")) {
                String[] name = username.split("\\s+");
                if (name != null && name[0] != null && !name[0].isEmpty())
                    return "You and " + checkNull(name[0]) + " are friends now";
                else
                    return "You both are friends";
            } else
                return "you both are friends";
        } else
            return "";
    }


    public void getMutualLikes(final String fb_id) {
        Bundle params = new Bundle();
        params.putString("fields", "context.fields(mutual_likes)");
        params.putString("limit", "25");
/* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + fb_id,
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            /* handle the result */
                        try {

                            if (response != null) {
                                Gson gson = new Gson();
                                JsonObject context = gson.fromJson(response.getJSONObject().getString("context"), JsonObject.class);
                                if (context != null) {
                                    JsonArray jsonArray = context.getAsJsonObject("mutual_likes").getAsJsonArray("data");
                                    if (jsonArray != null && jsonArray.size() > 0) {


                                        List<Chip> chipList = new ArrayList<>();
                                        List<Chip> mutualLikesChipList = new ArrayList<Chip>();
                                        mutuallikesChip.setChipBackgroundColor(getResources().getColor(R.color.base10));
                                        mutuallikesChip.setChipBackgroundColorSelected(getResources().getColor(R.color.base10));
                                        mutuallikesChip.setChipPadding(20);
                                        // chip.setChipBackgroundRes(R.drawable.black_border_white);


                                        for (int i = 0; i < jsonArray.size(); i++) {
                                            final String email = gson.fromJson(jsonArray.get(i).getAsJsonObject().toString(), MutualLikesObject.class).getName();

                                            mutualLikesChipList.add(new ChipTag(email));

                                            if (i == jsonArray.size() - 1) {
                                                mutuallikestitle.setVisibility(View.VISIBLE);
                                                mutuallikesChip.setVisibility(View.VISIBLE);
                                            }
                                        }

                                        mutuallikesChip.setChipList(mutualLikesChipList);
                                        progressBar.setVisibility(View.GONE);
                                        scrollView.setVisibility(View.VISIBLE);

                                        //   getLikes(fb_id);
                                    } else {
                                        mutuallikestitle.setVisibility(View.GONE);
                                        mutuallikesChip.setVisibility(View.GONE);
                                        progressBar.setVisibility(View.GONE);
                                        scrollView.setVisibility(View.VISIBLE);

                                    }
                                } else {
                                    progressBar.setVisibility(View.GONE);
                                    scrollView.setVisibility(View.VISIBLE);

                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                            scrollView.setVisibility(View.VISIBLE);

                        }

                    }
                }
        ).executeAsync();

    }

  /*  public void getLikes(String fb_id) {

        Bundle params = new Bundle();
        params.putInt("limit", 100);

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + fb_id + "/likes",
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {

                        try {


                            if (response != null) {
                                Gson gson = new Gson();
                                JsonArray jsonArray = gson.fromJson(response.getJSONObject().getString("data"), JsonArray.class);


                                if (jsonArray != null && jsonArray.size() > 0) {

                                    likesChipsView.setVisibility(View.VISIBLE);
                                    liketitle.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < jsonArray.size(); i++) {
                                        final String email = gson.fromJson(jsonArray.get(i).getAsJsonObject().toString(), MutualLikesObject.class).getName();
                                        final Uri imgUrl = Uri.parse("");
                                        final Contact contact = new Contact(null, null, null, email.toString(), imgUrl);

                                        Handler refresh = new Handler(Looper.getMainLooper());
                                        refresh.post(new Runnable() {
                                            public void run() {
                                                likesChipsView.addChip(email.toString(), imgUrl, contact);
                                            }
                                        });

                                    }

                                    progressBar.setVisibility(View.GONE);
                                    scrollView.setVisibility(View.VISIBLE);

                                } else {
                                    liketitle.setVisibility(View.GONE);
                                    likesChipsView.setVisibility(View.GONE);

                                    progressBar.setVisibility(View.GONE);
                                    scrollView.setVisibility(View.GONE);

                                }


                            } else {
                                progressBar.setVisibility(View.GONE);
                                scrollView.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            progressBar.setVisibility(View.GONE);
                            scrollView.setVisibility(View.GONE);
                            e.printStackTrace();
                        }


                    }
                }
        ).executeAsync();
    }*/

    public void getProfileDetails(final String fb_id) {


        if (AccessToken.getCurrentAccessToken() != null) {

            if (AccessToken.getCurrentAccessToken().getUserId() != null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {

                progressBar.setVisibility(View.VISIBLE);
                Map<String, String> params = new HashMap<String, String>();
                params.put("fb_id", AccessToken.getCurrentAccessToken().getUserId());
                params.put("userid", fb_id + "");
                params.put("access_token", AccessToken.getCurrentAccessToken().getToken());
                Call<LoginModel> cm = new RequestApi().getInterface().getUserInfo(params);

                cm.enqueue(new Callback<LoginModel>() {


                    @Override
                    public void onResponse(Response<LoginModel> response) {

                        Log.d("response", response.toString());
                        if (response != null && response.body() != null) {

                            final LoginModel user = response.body().getUser();
                            if (user != null) {
                                if (user.getLikes() != null && !user.getLikes().isEmpty()) {
                                    List<String> likes = Arrays.asList(user.getLikes().split(","));
                                    ;
                                    List<String> mutualLikes = new ArrayList<String>();
                                    printUserDetails(user, likes, mutualLikes);
                                } else {
                                    List<String> likes = new ArrayList<String>();
                                    ;
                                    List<String> mutualLikes = new ArrayList<String>();
                                    printUserDetails(user, likes, mutualLikes);
                                }
                            } else {
                                Toast.makeText(UserProfile_SomeActionActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(UserProfile_SomeActionActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        t.printStackTrace();
                    }


                });
            } else
                Log.d("fcmfromservice", "null string");
        } else
            Log.d("fcmfromservice", "null access token");

    }


    public String checkNull(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "NA";
    }

    public void printUserDetails(final LoginModel user, List<String> likes, List<String> mutualLikes) {
        try {


            if (user != null) {


                if (user.getCity() != null && !user.getCity().isEmpty()) {
                    location.setVisibility(View.VISIBLE);
                    location.setText("  " + checkNull(user.getCity()));
                    if (user.getCountry() != null && !user.getCountry().isEmpty()) {
                        location.setText(location.getText() + ", " + checkNull(user.getCountry().substring(0, 1).toUpperCase() + user.getCountry().substring(1) + ""));
                    }

                } else
                    location.setVisibility(View.GONE);


                if (user.getName() != null && !user.getName().isEmpty()) {

                    title.setVisibility(View.VISIBLE);
                    title.setText(checkNull(user.getName()));
                  /*  if (user.getAge() != null && !user.getAge().isEmpty()) {
                        if (Integer.parseInt(user.getAge()) > 0) {
                            title.setText(title.getText() + ", " + user.getAge());
                        } else {
                            title.setText(title.getText());

                        }


                    }*/

                    if (user.getBirthday() != null && !user.getBirthday().isEmpty()) {

                        try {
                            String[] dob = user.getBirthday().split("-");

                            if (dob[0] != "0000" && dob[1] != "00" && dob[2] != "00") {
                                int age = getAge(Integer.parseInt(dob[0]), Integer.parseInt(dob[1]), Integer.parseInt(dob[2]));
                                if (age > 0 && age<200) {
                                    title.setText(title.getText() + ", " + age);
                                } else {
                                    title.setText(title.getText());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                } else {
                    title.setVisibility(View.GONE);
                }


                if (user.getGender() != null && !user.getGender().isEmpty()) {
                    gender.setVisibility(View.VISIBLE);
                    gender.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                    gender.setText("  " + checkNull(user.getGender().substring(0, 1).toUpperCase() + user.getGender().substring(1) + ""));
                } else {
                    gender.setVisibility(View.GONE);
                }

                if (user.getAbout() != null && !user.getAbout().isEmpty()) {
                    aboutme.setVisibility(View.VISIBLE);
                    aboutme.setText(checkNull(user.getAbout() + ""));
                } else {
                    aboutme.setVisibility(View.GONE);
                }

                if (user.getLanguages() != null && !user.getLanguages().isEmpty()) {
                    languages.setVisibility(View.VISIBLE);
                    languagetitle.setVisibility(View.VISIBLE);
                    view.setVisibility(View.VISIBLE);
                    view1.setVisibility(View.VISIBLE);
                    languages.setText(checkNull(user.getLanguages().replace(",",", ") + ""));
                } else {
                    languages.setVisibility(View.GONE);
                    languagetitle.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    view1.setVisibility(View.GONE);
                }


                relationship.setText(checkNull(user.getRelationship_status()));
                hometown.setText(checkNull(user.getHometown() + ""));
                religion.setText(checkNull(user.getReligion() + ""));

                AdjustLayout(user);


                if (user.getProfile_url() != null && !user.getProfile_url().isEmpty()) {
               /*   Picasso.with(UserProfile_SomeActionActivity.this)
                            .load(user.getProfile_url())
                            .placeholder(getResources().getDrawable(R.mipmap.ic_person))
                            .into(profilepicture, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    // mPersonIcon.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError() {

                                }
                            });*/

                    FaceCrop faceCrop = new FaceCrop(this);
                    faceCrop.setFaceCropAsync(profilepicture, Uri.parse(Config.BaseImageUrl + user.getProfile_url()));
                    profilepicture.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    profilepicture.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showProfilePic(Config.BaseImageUrl + user.getProfile_url());
                        }
                    });
                } else {
                    profilepicture.setImageDrawable(getResources().getDrawable(R.drawable.ic_person_24dp));
                }

                if (likes != null && likes.size() > 0) {

                    List<Chip> chipList = new ArrayList<>();

                    chip.setChipBackgroundColor(getResources().getColor(R.color.base10));
                    chip.setChipBackgroundColorSelected(getResources().getColor(R.color.base10));
                    chip.setChipPadding(20);
                    // chip.setChipBackgroundRes(R.drawable.black_border_white);


                    for (int i = 0; i < likes.size(); i++) {
                        final String email = likes.get(i);
                        chipList.add(new ChipTag(email));
                        if (i == likes.size() - 1) {
                            liketitle.setVisibility(View.VISIBLE);
                            chip.setVisibility(View.VISIBLE);
                        }

                    }
                    chip.setChipList(chipList);

                } else {
                    liketitle.setVisibility(View.GONE);
                    chip.setVisibility(View.GONE);
                }


                getMutualLikes(user.getFbId());
               /* if (mutualLikes != null && mutualLikes.size() > 0) {

                    mutuallikestitle.setVisibility(View.VISIBLE);
                    mChipsView.setVisibility(View.VISIBLE);
                    for (int i = 0; i < mutualLikes.size(); i++) {
                        final String email = mutualLikes.get(i);
                        final Uri imgUrl = Uri.parse("https://robohash.org/" + Math.abs(email.hashCode()));
                        final Contact contact = new Contact(null, null, null, email, imgUrl);

                        Handler refresh = new Handler(Looper.getMainLooper());
                        refresh.post(new Runnable() {
                            public void run() {
                                mChipsView.addChip(email, imgUrl, contact);
                            }
                        });

                    }
                } else {
                    mutuallikestitle.setVisibility(View.GONE);
                    mChipsView.setVisibility(View.GONE);
                }
*/

               /* if (user.getEducation() != null && !user.getEducation().isEmpty()) {
                    job.setVisibility(View.VISIBLE);
                    company.setVisibility(View.VISIBLE);

                    String[] userJobDetails = user.getEducation().split("@");
                    if (userJobDetails != null) {
                        if (userJobDetails.length == 1) {
                            job.setVisibility(View.VISIBLE);
                            company.setVisibility(View.GONE);
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) job.getLayoutParams();
                            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                            job.setLayoutParams(params);
                            job.setText(checkNull(userJobDetails[0]) + "");
                        } else if (userJobDetails.length == 2) {
                            job.setVisibility(View.VISIBLE);
                            company.setVisibility(View.VISIBLE);
                            job.setText(checkNull(userJobDetails[0]) + "");
                            company.setText("@ " + checkNull(userJobDetails[1]) + "");
                        }
                    } else {
                        job.setVisibility(View.GONE);
                        company.setVisibility(View.GONE);
                    }

                } else {
                    job.setVisibility(View.GONE);
                    company.setVisibility(View.INVISIBLE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) location.getLayoutParams();
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    location.setLayoutParams(params);
                }*/

                if (user.getWork_company() != null && !user.getWork_company().isEmpty()) {
                    job.setVisibility(View.VISIBLE);
                    company.setVisibility(View.VISIBLE);
                    job.setText(checkNull(user.getWork_role()) + "");
                    company.setText("@ " + checkNull(user.getWork_company()) + "");
                } else {
                    if (user.getWork_role() != null && !user.getWork_role().isEmpty()) {
                        job.setVisibility(View.VISIBLE);
                        company.setVisibility(View.INVISIBLE);
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) job.getLayoutParams();
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                        job.setLayoutParams(params);
                        job.setText(checkNull(user.getWork_role()) + "");
                    } else {
                        job.setVisibility(View.INVISIBLE);
                        company.setVisibility(View.INVISIBLE);
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) location.getLayoutParams();
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                        location.setLayoutParams(params);
                    }
                }

            } else {
                scrollView.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
        }

    }

    public void showProfilePic(String url) {
        Dialog builder = new Dialog(UserProfile_SomeActionActivity.this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(UserProfile_SomeActionActivity.this);
        final ProgressBar progressBar = new ProgressBar(UserProfile_SomeActionActivity.this, null, android.R.attr.progressBarStyleLarge);
        Glide.with(UserProfile_SomeActionActivity.this).load(url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(imageView);

        progressBar.setVisibility(View.VISIBLE);

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.addContentView(progressBar, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.show();
    }


    public void acceptRequest(final String fb_id2) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //TODO your background code


                if (AccessToken.getCurrentAccessToken() != null) {

                    if (AccessToken.getCurrentAccessToken().getUserId() != null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {
                        ExpressInterestRequest expressInterestRequest = new ExpressInterestRequest();
                        expressInterestRequest.setFb_id2(fb_id2 + "");
                        expressInterestRequest.setFb_id1(AccessToken.getCurrentAccessToken().getUserId());

                        Call<ExpressInterestRequest> cm = new RequestApi().getInterface().acceptInterest(expressInterestRequest);

                        cm.enqueue(new Callback<ExpressInterestRequest>() {
                            @Override
                            public void onResponse(Response<ExpressInterestRequest> response) {

                                acceptrequest.setVisibility(View.GONE);
                                progressDialog.cancel();
                                mainActivityAlertFragmentModel.setFlag("3");
                                actionPerformed = true;
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                progressDialog.cancel();
                            }


                        });
                    } else
                        progressDialog.cancel();
                } else
                    progressDialog.cancel();

            }
        });
    }


    public void AdjustLayout(LoginModel user) {
        if (checkNull(user.getRelationship_status()).equalsIgnoreCase("na") && checkNull(user.getReligion()).equalsIgnoreCase("na") && user.getHometown().equalsIgnoreCase("na")) {
            relationshiplayout.setVisibility(View.GONE);
            religionlayout.setVisibility(View.GONE);
            hometownlayout.setVisibility(View.GONE);
        }

        if (checkNull(user.getRelationship_status()).equalsIgnoreCase("na")) {
            relationshiplayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.5f;
            religionlayout.setLayoutParams(params);
            relationshiplayout.setLayoutParams(params);

        }
        if (checkNull(user.getReligion()).equalsIgnoreCase("na")) {
            religionlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.5f;
            hometownlayout.setLayoutParams(params);
            relationshiplayout.setLayoutParams(params);

        }
        if (checkNull(user.getHometown()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.5f;
            religionlayout.setLayoutParams(params);
            relationshiplayout.setLayoutParams(params);
        }

        if (checkNull(user.getHometown()).equalsIgnoreCase("na") && checkNull(user.getReligion()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 3.0f;
            relationshiplayout.setLayoutParams(params);
        }

        if (checkNull(user.getHometown()).equalsIgnoreCase("na") && checkNull(user.getRelationship_status()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 3.0f;
            religionlayout.setLayoutParams(params);
        }

        if (checkNull(user.getReligion()).equalsIgnoreCase("na") && checkNull(user.getRelationship_status()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 3.0f;
            hometownlayout.setLayoutParams(params);
        }

    }

    @Override
    public void onBackPressed() {
        //

        if (actionPerformed) {
            Intent intent = new Intent();
            intent.putExtra("id", checkNull(mainActivityAlertFragmentModel.getFb_id2()));
            intent.putExtra("flag", checkNull(mainActivityAlertFragmentModel.getFlag()));
            setResult(3, intent);
            finish();//finishing activity
        } else {

            super.onBackPressed();
        }
    }


}
