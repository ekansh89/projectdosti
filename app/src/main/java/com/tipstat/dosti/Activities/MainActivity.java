package com.tipstat.dosti.Activities;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.firebase.messaging.FirebaseMessaging;

import com.tipstat.dosti.Fragments.BottoSheetFragment_Logout;
import com.tipstat.dosti.Fragments.MainAcitivtyPrimaryFragment;
import com.tipstat.dosti.Fragments.MainActivityListFragment;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.FcmIdRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Utils.NotificationUtils;
import com.tipstat.dosti.Views.FaceDetector.GlideFaceDetector;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


/**
 * This is the first activity user will be seen after succesfull login
 * here you can find list of new people, alerts,user profile and friends happen through project dosti
 */

public class MainActivity extends AppCompatActivity {
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    private ImageView logout, setpreferences;
    private TextView title;
    private MainAcitivtyPrimaryFragment mainAcitivtyPrimaryFragment;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GlideFaceDetector.initialize(MainActivity.this);
        /**
         *Setup the DrawerLayout and NavigationView
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        title = (TextView) findViewById(R.id.title);
        logout = (ImageView) findViewById(R.id.logout);
        setpreferences = (ImageView) findViewById(R.id.filter);


        setpreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this, SetPreferencesActivity.class), 526);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final BottoSheetFragment_Logout myBottomSheet = BottoSheetFragment_Logout.newInstance("Modal Bottom Sheet");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

            }
        });
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        /**
         * Lets inflate the very first fragment
         * Here , we are inflating the TabFragment as the first Fragment
         */

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mainAcitivtyPrimaryFragment = new MainAcitivtyPrimaryFragment();

        /**
         * fetching user name
         */
        Intent intent = getIntent();

        if (intent != null) {
            if (intent.hasExtra("name")) {
                Bundle bundle = new Bundle();
                bundle.putString("name", intent.getStringExtra("name"));
                mainAcitivtyPrimaryFragment.setArguments(bundle);
            } else if (intent.hasExtra("message")) {
                Log.d("from notification", true + "true");
                mainAcitivtyPrimaryFragment.moveViewPager();
            }

        }

        mFragmentTransaction.replace(R.id.containerView, mainAcitivtyPrimaryFragment).commit();

        /**
         * Setup click events on the Navigation View Items.
         */

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();


                if (menuItem.getItemId() == R.id.nav_item_sent) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerView, new MainActivityListFragment()).commit();

                }

                if (menuItem.getItemId() == R.id.nav_item_inbox) {
                    FragmentTransaction xfragmentTransaction = mFragmentManager.beginTransaction();
                    xfragmentTransaction.replace(R.id.containerView, new MainActivityListFragment()).commit();
                }

                return false;
            }

        });

        /**
         * Setup Drawer Toggle of the Toolbar
         */

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //  ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout, toolbar,R.string.app_name,
        //        R.string.app_name);

        // mDrawerLayout.setDrawerListener(mDrawerToggle);

        //  mDrawerToggle.syncState();

        displayFirebaseRegId();
        fcmBroadCastReceiver();
        //getFriendsList();


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);


        if (intent.hasExtra("message")) {
            Log.d("from notification", "true");
            mainAcitivtyPrimaryFragment.moveViewPager();
        } else {

           /* Log.d("from notification", "false");
            if(mainAcitivtyPrimaryFragment!=null)
            mainAcitivtyPrimaryFragment.moveViewPager();*/


        }

    }

    public void getFriendsList() {
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            /* handle the result */
                        Log.d("friendslist", response.toString());
                    }
                }
        ).executeAsync();
    }

    /**
     * function used for fcm broadcast receiver
     */

    public void fcmBroadCastReceiver() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    String title = intent.getStringExtra("title");
                    String timestamp = intent.getStringExtra("timestamp");
                    Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                    resultIntent.putExtra("message", message);
                    showNotificationMessage(MainActivity.this, "Project Dosti", message, timestamp, resultIntent);
                    Log.d("from notification", "true");
                }
            }
        };

    }

    /**
     * function used to update the fcm id of the user
     *
     * @param token fcm_id of the user
     */


    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e("Firebasenewtoiken", "sendRegistrationToServer: " + token);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //TODO your background code


                if (AccessToken.getCurrentAccessToken() != null) {

                    if (AccessToken.getCurrentAccessToken().getUserId() != null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {

                       /* TelephonyManager teleMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        String countryISOCode = "";

                        if (teleMgr != null) {
                            countryISOCode = teleMgr.getNetworkCountryIso();
                            if (teleMgr.getNetworkCountryIso().equalsIgnoreCase("in"))
                                countryISOCode = "india";
                            else
                                countryISOCode = "pakistan";
                        }*/


                        FcmIdRequest fcmIdRequest = new FcmIdRequest();
                        fcmIdRequest.setFb_id(AccessToken.getCurrentAccessToken().getUserId() + "");
                        // fcmIdRequest.setCountry(countryISOCode);
                        fcmIdRequest.setFcm(token);

                        Call cm = new RequestApi().getInterface().fcmregistartion(fcmIdRequest);

                        cm.enqueue(new Callback<FcmIdRequest>() {
                            @Override
                            public void onResponse(Response<FcmIdRequest> response) {

                            }

                            @Override
                            public void onFailure(Throwable t) {

                            }


                        });
                    } else
                        Log.d("fcmfromservice", "null string");
                } else
                    Log.d("fcmfromservice", "null access token");

            }
        });

    }

    /**
     * @param requestCode
     * @param resultCode
     * @param data
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //   mAdapter.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {

                if (fragment != null)
                    fragment.onActivityResult(requestCode, resultCode, data);

            }
        }
        Log.d("activity result act", "True");
    }

    /**
     * @param message
     */
    private void handleNotification(String message) {

        Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
        pushNotification.putExtra("message", message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

        // play notification sound
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        // notificationUtils.playNotificationSound();

    }

    /**
     * @param context
     * @param title
     * @param message
     * @param timeStamp
     * @param intent
     */

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        NotificationUtils notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * @return
     */


    public TextView returnTitleView() {
        return title;
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);


        if (!TextUtils.isEmpty(regId)) {
            Log.d("Firebase id", "Firebase reg id: " + regId);
            sendRegistrationToServer(regId);
        } else
            Log.d("Firebase id", "Firebase reg id not received yet " + regId);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("onresume", "true");
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        boolean notification = pref.getBoolean("notificationseen",true);

        if (!TextUtils.isEmpty(regId)) {
            Log.d("Firebase id", "Firebase reg id: " + regId);
            sendRegistrationToServer(regId);
        } else
            Log.d("Firebase id", "Firebase reg id not received yet " + regId);


        String startedFrom = getIntent().getStringExtra("message");
        Log.d("startedfrom", startedFrom + "\t"+notification);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GlideFaceDetector.releaseDetector();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}