package com.tipstat.dosti.Activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.AccessToken;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.tipstat.dosti.Models.ChipTag;
import com.tipstat.dosti.Models.LoginModel;
import com.tipstat.dosti.Models.MainActivityListFragmentModel;
import com.tipstat.dosti.Models.MutualLikesObject;
import com.tipstat.dosti.Models.SendRequestResponseModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.ExpressInterestRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.ChipView.ChipsView;
import com.tipstat.dosti.Views.ChipView.Contact;
import com.tipstat.dosti.Views.CustomProgressDialog;
import com.tipstat.dosti.Views.FaceDetector.FaceCrop;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


public class UserProfileActivity extends AppCompatActivity {


    private ImageView profilepicture;
    private TextView location, job, company, gender, languages, religion, relationship, hometown, aboutme, languagetitle, title;
    private ProgressBar progressBar;
    private CustomProgressDialog progressDialog;
    private ScrollView scrollView;
    private View view, view1;
    private RelativeLayout religionlayout, relationshiplayout, hometownlayout, sendrequestlayout;
    private TextView mutuallikestitle, likesTitle;
    private Button sendrequest;
    private TextView action;
    private MainActivityListFragmentModel user;
    private boolean actionPerformed = false;
    private ImageView back;
    private ChipView chip, mutuallikesChip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //  getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(" ");
        // getSupportActionBar().setIcon(R.mipmap.alvoff_logo_wb);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("listobject")) {


                title = (TextView) findViewById(R.id.title);
                profilepicture = (ImageView) findViewById(R.id.thumbnail);
                location = (TextView) findViewById(R.id.location);
                job = (TextView) findViewById(R.id.designation);
                company = (TextView) findViewById(R.id.organisation);
                gender = (TextView) findViewById(R.id.gender);
                aboutme = (TextView) findViewById(R.id.aboutme);
                hometown = (TextView) findViewById(R.id.hometown);
                relationship = (TextView) findViewById(R.id.relationship);
                languagetitle = (TextView) findViewById(R.id.languagetitle);
                this.view = findViewById(R.id.view);
                this.view1 = findViewById(R.id.view1);
                religion = (TextView) findViewById(R.id.religion);
                languages = (TextView) findViewById(R.id.language);
                action = (TextView) findViewById(R.id.action);
                scrollView = (ScrollView) findViewById(R.id.scrollView);
                relationshiplayout = (RelativeLayout) findViewById(R.id.relationshiplayout);
                religionlayout = (RelativeLayout) findViewById(R.id.religionlayout);
                hometownlayout = (RelativeLayout) findViewById(R.id.hometownlayout);
                sendrequestlayout = (RelativeLayout) findViewById(R.id.sendrequestlayout);
                progressBar = (ProgressBar) findViewById(R.id.progressBar1);
                sendrequest = (Button) findViewById(R.id.sendrequest);
                title = (TextView) findViewById(R.id.title);
                mutuallikestitle = (TextView) findViewById(R.id.mutualikestitle);
                likesTitle = (TextView) findViewById(R.id.likestitle);
                chip = (ChipView) findViewById(R.id.text_chip_attrs);
                mutuallikesChip = (ChipView) findViewById(R.id.text_chip_attrs_mutuallikes);

                back = (ImageView) findViewById(R.id.back);
                final Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_white_24px);
                upArrow.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
                back.setImageDrawable(upArrow);
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });


                user = (MainActivityListFragmentModel) intent.getSerializableExtra("listobject");
                if (user != null && user.getFbId() != null) {

                    getProfileDetails(user.getFbId());
                } else {
                    scrollView.setVisibility(View.GONE);
                    Toast.makeText(UserProfileActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }


            }
        } else {
            Toast.makeText(UserProfileActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            scrollView.setVisibility(View.GONE);
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    public void getProfileDetails(final String fb_id) {


        if (AccessToken.getCurrentAccessToken() != null) {

            if (AccessToken.getCurrentAccessToken().getUserId() != null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {

                progressBar.setVisibility(View.VISIBLE);
                Map<String, String> params = new HashMap<String, String>();
                params.put("fb_id", AccessToken.getCurrentAccessToken().getUserId());
                params.put("userid", fb_id + "");
                params.put("access_token", AccessToken.getCurrentAccessToken().getToken());
                Call<LoginModel> cm = new RequestApi().getInterface().getUserInfo(params);

                cm.enqueue(new Callback<LoginModel>() {


                    @Override
                    public void onResponse(Response<LoginModel> response) {

                        Log.d("response", response.toString());
                        if (response != null && response.body() != null) {

                            final LoginModel user = response.body().getUser();
                            if (user != null) {
                                if (user.getLikes() != null && !user.getLikes().isEmpty()) {

                                    List<String> mutualLikes = new ArrayList<String>();
                                    printUserDetails(user, user.getLikes(), mutualLikes);
                                } else {
                                    List<String> likes = new ArrayList<String>();
                                    ;
                                    List<String> mutualLikes = new ArrayList<String>();
                                    printUserDetails(user, user.getLikes(), mutualLikes);
                                }
                            } else {
                                Toast.makeText(UserProfileActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(UserProfileActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        t.printStackTrace();
                    }


                });
            } else
                Log.d("fcmfromservice", "null string");
        } else
            Log.d("fcmfromservice", "null access token");

    }

    public void printUserDetails(final LoginModel user, String user_likes, List<String> user_mutuallikes) {
        try {


            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);

            List<String> mutualLikes = user_mutuallikes;
            List<String> likes = Arrays.asList(user.getLikes().split(","));


            if (user.getFlag() != null && (user.getFlag().equalsIgnoreCase("3"))) {
                sendrequestlayout.setVisibility(View.VISIBLE);
                sendrequestlayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                sendrequest.setTextColor(getResources().getColor(android.R.color.white));
                sendrequest.setText("  Facebook");

                sendrequest.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.facebook), null, null, null);
                action.setVisibility(View.VISIBLE);
                action.setText(returnStatus(user.getFlag(), user.getName()));

                sendrequest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(UserProfileActivity.this, webview.class);
                        intent.putExtra("url", "http://www.facebook.com/" + user.getFbId());
                        startActivity(intent);

                    }
                });

                sendrequestlayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(UserProfileActivity.this, webview.class);
                        intent.putExtra("url", "http://www.facebook.com/" + user.getFbId());
                        startActivity(intent);
                    }
                });
            } else if (user.getFlag() != null && user.getFlag().equalsIgnoreCase("4")) {
                sendrequestlayout.setVisibility(View.VISIBLE);
                sendrequestlayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                sendrequest.setTextColor(getResources().getColor(android.R.color.white));
                sendrequest.setText("  Share Friendship on Facebook");

                sendrequest.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.facebook), null, null, null);
                action.setVisibility(View.VISIBLE);
                action.setText(returnStatus(user.getFlag(), user.getName()));

                sendrequest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final ShareDialog shareDialog = new ShareDialog(UserProfileActivity.this);
                        if (ShareDialog.canShow(ShareLinkContent.class)) {

                            ShareLinkContent content = new ShareLinkContent.Builder()
                                    .setContentUrl(Uri.parse("http://www.projectdosti.com/"))
                                    .setContentTitle("Project Dosti")
                                    .setImageUrl(Uri.parse("http://www.projectdosti.com/img/logo.png"))
                                    .setContentDescription("I met " + user.getName() + " through Project Dosti")
                                    .build();
                            shareDialog.show(content);

                        } else {
                            Intent intent1 = new Intent(UserProfileActivity.this, webview.class);
                            intent1.putExtra("url", "http://www.facebook.com/" + user.getFbId());
                            startActivity(intent1);
                        }
                    }
                });

                sendrequestlayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final ShareDialog shareDialog = new ShareDialog(UserProfileActivity.this);
                        if (ShareDialog.canShow(ShareLinkContent.class)) {

                            ShareLinkContent content = new ShareLinkContent.Builder()
                                    .setContentUrl(Uri.parse("http://www.projectdosti.com/"))
                                    .setContentTitle("Project Dosti")
                                    .setImageUrl(Uri.parse("http://www.projectdosti.com/img/logo.png"))
                                    .setContentDescription("I met " + user.getName() + " through Project Dosti")
                                    .build();
                            shareDialog.show(content);

                        } else {
                            Intent intent1 = new Intent(UserProfileActivity.this, webview.class);
                            intent1.putExtra("url", "http://www.facebook.com/" + user.getFbId());
                            startActivity(intent1);
                        }
                    }
                });


            } else if (user.getFlag() != null && user.getFlag().equalsIgnoreCase("1")) {
                action.setVisibility(View.VISIBLE);
                action.setText(returnStatus(user.getFlag(), user.getName()));
                sendrequestlayout.setVisibility(View.GONE);

            } else if (user.getFlag() != null && user.getFlag().equalsIgnoreCase("2")) {
                action.setVisibility(View.VISIBLE);
                action.setText(returnStatus(user.getFlag(), user.getName()));
                sendrequestlayout.setVisibility(View.GONE);
            } else {

                action.setVisibility(View.GONE);
                sendrequestlayout.setVisibility(View.VISIBLE);
                sendrequest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressDialog = CustomProgressDialog.show(UserProfileActivity.this);
                        sendRequest(user.getFbId());

                    }


                });

                sendrequestlayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progressDialog = CustomProgressDialog.show(UserProfileActivity.this);
                        sendRequest(user.getFbId());
                    }
                });
            }


            if (user.getName() != null && !user.getName().isEmpty()) {

                title.setVisibility(View.VISIBLE);
                title.setText(checkNull(user.getName()));
               /* if (user.getAge() != null && !user.getAge().isEmpty()) {


                    if (user != null && user.getAge() != null) {

                        if (Integer.parseInt(user.getAge()) > 0) {
                            title.setText(title.getText() + ", " + user.getAge());
                        } else {
                            title.setText(title.getText());
                        }
                    }


                }*/

                if (user.getBirthday() != null && !user.getBirthday().isEmpty()) {

                    try {
                        String[] dob = user.getBirthday().split("-");

                        if (dob[0] != "0000" && dob[1] != "00" && dob[2] != "00") {
                            int age = getAge(Integer.parseInt(dob[0]), Integer.parseInt(dob[1]), Integer.parseInt(dob[2]));
                            if (age > 0 && age < 200) {
                                title.setText(title.getText() + ", " + age);
                            } else {
                                title.setText(title.getText());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } else {
                title.setVisibility(View.GONE);
            }
            if (user.getCity() != null && !user.getCity().isEmpty()) {
                location.setVisibility(View.VISIBLE);
                location.setText("  " + checkNull(user.getCity()));
                if (user.getCountry() != null && !user.getCountry().isEmpty()) {
                    location.setText(location.getText() + ", " + checkNull(user.getCountry().substring(0, 1).toUpperCase() + user.getCountry().substring(1) + ""));
                }

            } else {
                location.setVisibility(View.INVISIBLE);
                location.setCompoundDrawables(null, null, null, null);
            }
            if (user.getGender() != null && !user.getGender().isEmpty()) {
                gender.setVisibility(View.VISIBLE);
                gender.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                gender.setText("  " + checkNull(user.getGender().substring(0, 1).toUpperCase() + user.getGender().substring(1) + ""));
            } else {
                gender.setVisibility(View.GONE);
            }

            if (user.getAbout() != null && !user.getAbout().isEmpty()) {
                aboutme.setVisibility(View.VISIBLE);
                aboutme.setText(checkNull(user.getAbout() + ""));
            } else {
                aboutme.setVisibility(View.GONE);
            }

            if (user.getLanguages() != null && !user.getLanguages().isEmpty()) {
                languages.setVisibility(View.VISIBLE);
                languagetitle.setVisibility(View.VISIBLE);
                view.setVisibility(View.VISIBLE);
                view1.setVisibility(View.VISIBLE);
                languages.setText(checkNull(user.getLanguages().replace(",", ", ") + ""));
            } else {
                languages.setVisibility(View.GONE);
                languagetitle.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                view1.setVisibility(View.GONE);
            }


            relationship.setText(checkNull(user.getRelationship_status()));
            hometown.setText(checkNull(user.getHometown() + ""));
            religion.setText(checkNull(user.getReligion() + ""));

            AdjustLayout(user);


            if (user.getProfile_url() != null && !user.getProfile_url().isEmpty()) {
                        /*Picasso.with(UserProfileActivity.this)
                                .load(user.getProfile_url())
                                .placeholder(getResources().getDrawable(R.mipmap.ic_person))
                                .into(profilepicture, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {
                                        // mPersonIcon.setVisibility(View.INVISIBLE);
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });*/

                      /*  Glide.with(UserProfileActivity.this)
                                .load(user.getProfile_url())
                                .transform(new FaceCrop())
                                .into(profilepicture);*/

                FaceCrop faceCrop = new FaceCrop(this);
                faceCrop.setFaceCropAsync(profilepicture, Uri.parse(Config.BaseImageUrl + user.getProfile_url()));

                profilepicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showProfilePic(Config.BaseImageUrl + user.getProfile_url());
                    }
                });


            }

                 /*   if (mutualLikes != null && mutualLikes.size() > 0) {

                        mutuallikestitle.setVisibility(View.VISIBLE);
                        mChipsView.setVisibility(View.VISIBLE);
                        for (int i = 0; i < mutualLikes.size(); i++) {
                            final String email = mutualLikes.get(i);
                            final Uri imgUrl = Uri.parse("https://robohash.org/" + Math.abs(email.hashCode()));
                            final Contact contact = new Contact(null, null, null, email, imgUrl);

                            mChipsView.addChip(email, imgUrl, contact);

                        }
                    } else {
                        mutuallikestitle.setVisibility(View.GONE);
                        mChipsView.setVisibility(View.GONE);
                    }*/


            getMutualLikes(user.getFbId(), likes);


          /*  if (user.getEducation() != null && !user.getEducation().isEmpty()) {
                job.setVisibility(View.VISIBLE);
                company.setVisibility(View.VISIBLE);
                String[] userJobDetails = user.getEducation().split("@");
                if (userJobDetails != null) {
                    if (userJobDetails.length == 1) {
                        job.setVisibility(View.VISIBLE);
                        company.setVisibility(View.GONE);
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) job.getLayoutParams();
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                        job.setLayoutParams(params);
                        job.setText(checkNull(userJobDetails[0]) + "");
                    } else if (userJobDetails.length == 2) {
                        job.setVisibility(View.VISIBLE);
                        company.setVisibility(View.VISIBLE);
                        job.setText(checkNull(userJobDetails[0]) + "");
                        company.setText("@ " + checkNull(userJobDetails[1]) + "");
                    }
                } else {
                    job.setVisibility(View.GONE);
                    company.setVisibility(View.GONE);
                }

            } else {
                job.setVisibility(View.GONE);
                company.setVisibility(View.GONE);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) location.getLayoutParams();
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                location.setLayoutParams(params);
            }
*/

            if (user.getWork_company() != null && !user.getWork_company().isEmpty()) {
                job.setVisibility(View.VISIBLE);
                company.setVisibility(View.VISIBLE);
                job.setText(checkNull(user.getWork_role()) + "");
                company.setText("@ " + checkNull(user.getWork_company()) + "");
            } else {
                if (user.getWork_role() != null && !user.getWork_role().isEmpty()) {
                    job.setVisibility(View.VISIBLE);
                    company.setVisibility(View.INVISIBLE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) job.getLayoutParams();
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    job.setLayoutParams(params);
                    job.setText(checkNull(user.getWork_role()) + "");
                } else {
                    job.setVisibility(View.INVISIBLE);
                    company.setVisibility(View.INVISIBLE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) location.getLayoutParams();
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    location.setLayoutParams(params);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            Log.d("incatch", "yes");
        }
    }

    public int getAge(int _year, int _month, int _day) {

        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, a;

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);
        a = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH))
                || ((m == cal.get(Calendar.MONTH)) && (d < cal
                .get(Calendar.DAY_OF_MONTH)))) {
            --a;
        }
        if (a < 0)
            throw new IllegalArgumentException("Age < 0");
        return a;
    }

    public void getMutualLikes(final String fb_id, final List<String> likes) {
        Bundle params = new Bundle();
        params.putString("fields", "context.fields(mutual_likes)");
        params.putString("limit", "25");
/* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + fb_id,
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            /* handle the result */
                        try {

                            if (likes != null && likes.size() > 0) {


                                List<Chip> chipList = new ArrayList<>();

                                chip.setChipBackgroundColor(getResources().getColor(R.color.base10));
                                chip.setChipBackgroundColorSelected(getResources().getColor(R.color.base10));
                                chip.setChipPadding(20);
                                // chip.setChipBackgroundRes(R.drawable.black_border_white);


                                for (int i = 0; i < likes.size(); i++) {
                                    final String email = likes.get(i);
                                    chipList.add(new ChipTag(email));
                                    if (likes.size() > 0 && i == likes.size() - 1) {
                                        likesTitle.setVisibility(View.VISIBLE);
                                        chip.setVisibility(View.VISIBLE);
                                    }

                                }
                                chip.setChipList(chipList);
                            } else {
                                likesTitle.setVisibility(View.GONE);
                                chip.setVisibility(View.GONE);
                            }


                            if (response != null) {


                                Gson gson = new Gson();
                                JsonObject context = gson.fromJson(response.getJSONObject().getString("context"), JsonObject.class);
                                if (context != null) {
                                    JsonArray jsonArray = context.getAsJsonObject("mutual_likes").getAsJsonArray("data");
                                    if (jsonArray != null && jsonArray.size() > 0) {


                                        List<Chip> chipList = new ArrayList<>();
                                        List<Chip> mutualLikesChipList = new ArrayList<Chip>();
                                        mutuallikesChip.setChipBackgroundColor(getResources().getColor(R.color.base10));
                                        mutuallikesChip.setChipBackgroundColorSelected(getResources().getColor(R.color.base10));
                                        mutuallikesChip.setChipPadding(20);

                                        for (int i = 0; i < jsonArray.size(); i++) {


                                            final String email = gson.fromJson(jsonArray.get(i).getAsJsonObject().toString(), MutualLikesObject.class).getName();

                                            mutualLikesChipList.add(new ChipTag(email));

                                            if (i == jsonArray.size() - 1) {
                                                mutuallikestitle.setVisibility(View.VISIBLE);
                                                mutuallikesChip.setVisibility(View.VISIBLE);
                                            }

                                        }
                                        mutuallikesChip.setChipList(mutualLikesChipList);
                                        progressBar.setVisibility(View.GONE);
                                        scrollView.setVisibility(View.VISIBLE);

                                        //   getLikes(fb_id);
                                    } else {
                                        mutuallikestitle.setVisibility(View.GONE);
                                        mutuallikesChip.setVisibility(View.GONE);
                                        progressBar.setVisibility(View.GONE);

                                    }
                                } else {
                                    progressBar.setVisibility(View.GONE);

                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);

                        }

                    }
                }
        ).executeAsync();

        if (likes == null || likes.size() < 0) {
            likesTitle.setVisibility(View.GONE);
            chip.setVisibility(View.GONE);
        }

    }

    public String returnStatus(String flag, String username) {

        Log.d("flag", flag);
        if (flag.equalsIgnoreCase("2")) {
            return username + " has expressed interest";
        } else if (flag.equalsIgnoreCase("1")) {
            return "You have expressed interest on " + username;
        } else if (flag.equalsIgnoreCase("3"))
            return username + " and you are matched";
        else if (flag.equalsIgnoreCase("4")) {
            String[] name = username.split("\\s+");
            if (name != null && name[0] != null && !name[0].isEmpty())
                return "You and " + checkNull(name[0]) + " are friends now";
            else
                return "You both are friends";
        } else
            return "you both are friends";

    }

    @Override
    public void onBackPressed() {
        //

        if (actionPerformed) {

            Calendar rightNow = Calendar.getInstance();
          /*  // offset to add since we're not UTC
            long offset = rightNow.get(Calendar.ZONE_OFFSET) +
                    rightNow.get(Calendar.DST_OFFSET);
            long timeinmills = (rightNow.getTimeInMillis() + offset) %
                    (24 * 60 * 60 * 1000);*/

            Date temp = rightNow.getTime();
            String dateTemp = temp.getYear() + "-" + temp.getMonth() + "-" + temp.getDate() + " " + temp.getHours() + ":" + temp.getMinutes() + ":" + temp.getSeconds();
            Log.d("Datetemp", dateTemp + "");
            Intent intent = new Intent();
            intent.putExtra("id", checkNull(user.getFbId()));
            intent.putExtra("name", checkNull(user.getName()));
            intent.putExtra("gender", checkNull(user.getGender()));
            intent.putExtra("Age", checkNull(user.getAge()));
            intent.putExtra("time", checkNull(dateTemp + ""));
            intent.putExtra("profilepic", checkNull(Config.BaseImageUrl + user.getProfile_url()));
            intent.putExtra("flag", checkNull(user.getFlag()));
            setResult(2, intent);
            finish();//finishing activity
        } else {

            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void AdjustLayout(LoginModel user) {
        if (checkNull(user.getRelationship_status()).equalsIgnoreCase("na") && checkNull(user.getReligion()).equalsIgnoreCase("na") && user.getHometown().equalsIgnoreCase("na")) {
            relationshiplayout.setVisibility(View.GONE);
            religionlayout.setVisibility(View.GONE);
            hometownlayout.setVisibility(View.GONE);
        }

        if (checkNull(user.getRelationship_status()).equalsIgnoreCase("na")) {
            relationshiplayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.5f;
            religionlayout.setLayoutParams(params);
            relationshiplayout.setLayoutParams(params);

        }
        if (checkNull(user.getReligion()).equalsIgnoreCase("na")) {
            religionlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.5f;
            hometownlayout.setLayoutParams(params);
            relationshiplayout.setLayoutParams(params);

        }
        if (checkNull(user.getHometown()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.5f;
            religionlayout.setLayoutParams(params);
            relationshiplayout.setLayoutParams(params);
        }

        if (checkNull(user.getHometown()).equalsIgnoreCase("na") && checkNull(user.getReligion()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 3.0f;
            relationshiplayout.setLayoutParams(params);
        }

        if (checkNull(user.getHometown()).equalsIgnoreCase("na") && checkNull(user.getRelationship_status()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 3.0f;
            religionlayout.setLayoutParams(params);
        }

        if (checkNull(user.getReligion()).equalsIgnoreCase("na") && checkNull(user.getRelationship_status()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 3.0f;
            hometownlayout.setLayoutParams(params);
        }

    }

    public String checkNull(String string) {

        if (string != null && !string.isEmpty()) {
            return string;
        } else
            return "NA";
    }


    public void sendRequest(final String fb_id2) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //TODO your background code


                if (AccessToken.getCurrentAccessToken() != null) {

                    if (AccessToken.getCurrentAccessToken().getUserId() != null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {
                        ExpressInterestRequest expressInterestRequest = new ExpressInterestRequest();
                        expressInterestRequest.setFb_id1(AccessToken.getCurrentAccessToken().getUserId() + "");
                        expressInterestRequest.setFb_id2(fb_id2);

                        Call<SendRequestResponseModel> cm = new RequestApi().getInterface().expressinterest(expressInterestRequest);

                        cm.enqueue(new Callback<SendRequestResponseModel>() {
                            @Override
                            public void onResponse(Response<SendRequestResponseModel> response) {
                                progressDialog.cancel();
                                try {
                                    if (response != null && response.body() != null && response.body().getStatus() != null && response.body().getStatus().equalsIgnoreCase("1")) {
                                        Toast.makeText(UserProfileActivity.this, "Request sent", Toast.LENGTH_SHORT).show();
                                        user.setFlag("1");
                                        actionPerformed = true;
                                        sendrequestlayout.setVisibility(View.GONE);
                                    } else {
                                        Toast.makeText(UserProfileActivity.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                                        sendrequestlayout.setVisibility(View.VISIBLE);
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(UserProfileActivity.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                                    sendrequestlayout.setVisibility(View.VISIBLE);

                                }

                            }

                            @Override
                            public void onFailure(Throwable t) {
                                Toast.makeText(UserProfileActivity.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                                sendrequestlayout.setVisibility(View.VISIBLE);
                                progressDialog.cancel();
                            }


                        });
                    } else {
                        Toast.makeText(UserProfileActivity.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                        sendrequestlayout.setVisibility(View.VISIBLE);
                    }

                } else {
                    Toast.makeText(UserProfileActivity.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                    sendrequestlayout.setVisibility(View.VISIBLE);
                }


            }
        });
    }

    public void showProfilePic(String url) {
        Dialog builder = new Dialog(UserProfileActivity.this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(UserProfileActivity.this);
        final ProgressBar progressBar = new ProgressBar(UserProfileActivity.this, null, android.R.attr.progressBarStyleLarge);
        Glide.with(UserProfileActivity.this).load(url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(imageView);

        progressBar.setVisibility(View.VISIBLE);

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.addContentView(progressBar, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.show();
    }
}
