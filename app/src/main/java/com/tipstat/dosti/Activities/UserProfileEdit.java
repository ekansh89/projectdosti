package com.tipstat.dosti.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.AccessToken;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.identity.intents.Address;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tipstat.dosti.Adapters.LocationAdapter;
import com.tipstat.dosti.Adapters.MainActivityFriendsListFragmentAdapter;
import com.tipstat.dosti.Fragments.BottomSheetFragment_ChooseImage;
import com.tipstat.dosti.Fragments.BottomSheetSelectionFragment;
import com.tipstat.dosti.Fragments.MainActivityFriendsListFragment;
import com.tipstat.dosti.Models.LocationModel;
import com.tipstat.dosti.Models.LoginModel;
import com.tipstat.dosti.Models.SendRequestResponseModel;
import com.tipstat.dosti.Models.UserAttributesModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.SetPreferncesRequest;
import com.tipstat.dosti.RequestModels.UpdateProfileRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Retrofit.VolleyMultipartRequest;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.CircularImageview;
import com.tipstat.dosti.Views.CustomProgressDialog;
import com.tipstat.dosti.Views.FaceDetector.FaceCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;


import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

import static com.tipstat.dosti.R.id.date;

public class UserProfileEdit extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    private static final int PIC_CROP = 526;
    private EditText name;
    private EditText dob;
    private EditText gender;
    private EditText religion;
    private EditText relationship;
    private EditText languages;
    private EditText city;
    private EditText aboutme;
    private FloatingActionButton chooseImage;
    private CircularImageview profilepic;
    private String SERVER_URL = Config.BaseUrl+"dosti/v1/uploadImage";
    private Uri fileUri;
    public static int MEDIA_TYPE_IMAGE = 22;
    public static String IMAGE_DIRECTORY_NAME = "Avloff";
    private int PICK_IMAGES = 1;
    final int SELECT_PICTURE = 2, TAKE_PICTURE = 3;
    private Bitmap thumbnail_r;
    private static final int REQUEST_WRITE_STORAGE = 112;
    private Button updateProfile;
    private CustomProgressDialog progressDialog;
    private ImageView back;
    private boolean actionperformed = false;
    private LoginModel user;
    private List<String> language_list = new ArrayList<>(), r_status_list = new ArrayList<>(), religion_list = new ArrayList<>();
    private Uri croppedImageUri;
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int PERMISSION_REQUEST_CODE = 1;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Location mLastLocation;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters
    private CustomProgressDialog customProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(" ");
        // getSupportActionBar().setIcon(R.mipmap.alvoff_logo_wb);
        //   getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        profilepic = (CircularImageview) findViewById(R.id.fab);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Log.e("Alert", "Lets See if it Works !!!");
                paramThrowable.printStackTrace();
            }
        });


        name = (EditText) findViewById(R.id.username);
        dob = (EditText) findViewById(R.id.dob);
        gender = (EditText) findViewById(R.id.gender);
        religion = (EditText) findViewById(R.id.religion);
        relationship = (EditText) findViewById(R.id.relationship);
        languages = (EditText) findViewById(R.id.language);
        city = (EditText) findViewById(R.id.address);
        aboutme = (EditText) findViewById(R.id.aboutme);
        updateProfile = (Button) findViewById(R.id.update);
        chooseImage = (FloatingActionButton) findViewById(R.id.edit);
        back = (ImageView) findViewById(R.id.back);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_white_24px);
        upArrow.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        back.setImageDrawable(upArrow);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getUserAtrributes();
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("userobject")) {

            user = (LoginModel) intent.getSerializableExtra("userobject");
            if (user != null) {
                name.setText("  " + checkNull(user.getName()));
                dob.setText("  " + checkNull(getDOB(user.getBirthday())));
                gender.setText("  " + checkNull(user.getGender().substring(0, 1).toUpperCase() + user.getGender().substring(1)));
                religion.setText(checkNull(user.getReligion()));
                relationship.setText(checkNull(user.getRelationship_status()));
                languages.setText(checkNull(user.getLanguages().replace(",",", ")));
                city.setText("  " + checkNull(user.getCity()));


                city.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        final int DRAWABLE_LEFT = 0;
                        final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
                        final int DRAWABLE_BOTTOM = 3;

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (event.getRawX() >= (city.getRight() - city.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                // your action here

                                if (ActivityCompat.checkSelfPermission(UserProfileEdit.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UserProfileEdit.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling
                                    //    ActivityCompat#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for ActivityCompat#requestPermissions for more details.

                                    ActivityCompat.requestPermissions(UserProfileEdit.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);

                                    return false;
                                }


                                // First we need to check availability of play services
                                if (checkPlayServices()) {

                                    // Building the GoogleApi client
                                    customProgressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                                    buildGoogleApiClient();
                                }


                                return true;
                            }
                        }
                        return false;
                    }
                });


                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(UserProfileEdit.this, "Please change Facebook information to update this field", Toast.LENGTH_SHORT).show();
                    }
                });
                dob.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(UserProfileEdit.this, "Please change Facebook information to update this field", Toast.LENGTH_SHORT).show();
                    }
                });
                gender.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(UserProfileEdit.this, "Please change Facebook information to update this field", Toast.LENGTH_SHORT).show();
                    }
                });
                city.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(UserProfileEdit.this, "Please change Facebook information to update this field", Toast.LENGTH_SHORT).show();
                    }
                });

                chooseImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        boolean hasPermission = (ContextCompat.checkSelfPermission(UserProfileEdit.this,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

                        if (!hasPermission) {
                            ActivityCompat.requestPermissions(UserProfileEdit.this,
                                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                                    REQUEST_WRITE_STORAGE);
                        } else {

                            final BottomSheetFragment_ChooseImage myBottomSheet = BottomSheetFragment_ChooseImage.newInstance("Modal Bottom Sheet");
                            myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                        }

                    }
                });


                updateProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                        updateDetails(user.getBirthday());

                    }
                });

                profilepic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        showProfilePic(Config.BaseImageUrl + user.getProfile_url());

                    }
                });

                aboutme.setText(checkNull(user.getAbout()));
                if (user.getProfile_url() != null && !user.getProfile_url().isEmpty()) {
                   /* Picasso.with(UserProfileEdit.this)
                            .load(user.getProfile_url())
                            .placeholder(getResources().getDrawable(R.mipmap.ic_person))
                            .into(profilepic, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    // mPersonIcon.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError() {

                                }
                            });*/

                    FaceCrop faceCrop = new FaceCrop(this);
                    faceCrop.setFaceCropAsync(profilepic, Uri.parse(Config.BaseImageUrl + user.getProfile_url()));

                    religion.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(religion_list, "religion");
                            myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                        }
                    });


                    relationship.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(r_status_list, "relationship");
                            myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                        }
                    });


                    languages.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                          String temp = languages.getText().toString().replace(", ",",");
                            final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(language_list, "language", temp.split(","));
                            myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                        }
                    });


                } else {
                    profilepic.setImageDrawable(getResources().getDrawable(R.drawable.ic_person_24dp));
                }


            }


        }


    }

    public void getFacebookALbums() {
        final String temp = "/" + AccessToken.getCurrentAccessToken().getApplicationId() + "/albums";
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/1242081909167601/photos",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {

                        Log.d("Response", response.toString() + "\t" + temp + "\t" + AccessToken.getCurrentAccessToken().getToken() + "\t" + AccessToken.getCurrentAccessToken().getUserId());
                    }
                }
        ).executeAsync();

    }

    public void getUserAtrributes() {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(UserProfileEdit.this);
        SharedPreferences pref = getSharedPreferences(Config.SHARED_PREF, 0);
        String userpreferences = pref.getString("userpreferences", null);
        String userCountry = pref.getString("usercountry", null);

        Map<String, String> params = new HashMap<String, String>();
        if (userCountry != null && !userCountry.isEmpty() && (userCountry.equalsIgnoreCase("india") || userCountry.equalsIgnoreCase("pakistan") || userCountry.equalsIgnoreCase("pakisthan"))) {
            if (userCountry.equalsIgnoreCase("india"))
                params.put("country", "pakistan");
            else
                params.put("country", "india");
        } else {
            Gson gson = new Gson();
            SetPreferncesRequest setPreferncesRequests = gson.fromJson(userpreferences, SetPreferncesRequest.class);
            if (setPreferncesRequests != null && setPreferncesRequests.getCountry() != null && !setPreferncesRequests.getCountry().isEmpty()) {
                if (setPreferncesRequests.getCountry().equalsIgnoreCase("pakistan"))
                    params.put("country", "pakistan");
                else params.put("country", "india");
            } else {
                params.put("country", "");
            }
        }

        Call cm = new RequestApi().getInterface().user_attributes(params);

        cm.enqueue(new Callback<UserAttributesModel>() {
            @Override
            public void onResponse(Response<UserAttributesModel> response) {

                try {


                    customProgressDialog.cancel();
                    if (response != null && response.body() != null) {
                        if (response.body().getLanguage() != null && response.body().getLanguage().size() > 0) {
                            language_list.addAll(response.body().getLanguage());
                        }

                        if (response.body().getR_status() != null && response.body().getR_status().size() > 0) {
                            r_status_list.addAll(response.body().getR_status());
                        }

                        if (response.body().getReligion() != null && response.body().getReligion().size() > 0) {
                            religion_list.addAll(response.body().getReligion());
                        }
                    }
                } catch (Exception e) {
                    customProgressDialog.cancel();
                }

            }

            @Override
            public void onFailure(Throwable t) {
                customProgressDialog.cancel();
            }


        });

    }


    @Override
    public void onBackPressed() {
        if (actionperformed) {
            Intent intent = new Intent();
            intent.putExtra("userobject", user);
            setResult(501, intent);
            finish();


        } else {
            super.onBackPressed();
        }
    }

    public void showProfilePic(String url) {
        Dialog builder = new Dialog(UserProfileEdit.this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(UserProfileEdit.this);
        final ProgressBar progressBar = new ProgressBar(UserProfileEdit.this, null, android.R.attr.progressBarStyleLarge);
        Log.d("imageurl", url);
        Glide.with(UserProfileEdit.this).load(url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(imageView);

        progressBar.setVisibility(View.VISIBLE);

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.addContentView(progressBar, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.show();
    }

    public String getDOB(String birthday) {
        if (birthday != null && !birthday.isEmpty()) {
            String[] temp = birthday.split("-");

            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-mm-dd");


            Date date = new Date();
            try {
             /*   date = inputFormat.parse(birthday);
                Log.d("date",date.getMonth()+"");
                String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);*/
                return temp[2] + " " + theMonth(Integer.parseInt(temp[1])-1) + " " + temp[0];
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "";
            }

        } else {
            return "";
        }
    }

    public static String theMonth(int month) {
        String[] monthNames = {"Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"};
        return monthNames[month];
    }

    public void setText(String type, String selectedText) {
        if (type.equalsIgnoreCase("religion")) {
            religion.setText(selectedText);

        } else if (type.equalsIgnoreCase("relationship")) {
            relationship.setText(selectedText);

        } else if (type.equalsIgnoreCase("language")) {
            languages.setText(selectedText.replace(",",", "));

        }
    }

    /**
     *
     */
    public void takePicture() {

        Intent intent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                /*
                 * File photo = new
                 * File(Environment.getExternalStorageDirectory(),
                 * "Pic.jpg"); intent.putExtra(MediaStore.EXTRA_OUTPUT,
                 * Uri.fromFile(photo)); imageUri = Uri.fromFile(photo);
                 */
        // startActivityForResult(intent,TAKE_PICTURE);

        Intent intents = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intents.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intents, TAKE_PICTURE);
    }


    public void selectgImageFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                SELECT_PICTURE);

    }

    public String checkNull(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else return "";

    }
  /*  private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            profilepic.setImageURI(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }*/

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap thePic = null;
                try {
                    thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);


                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    thePic.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    byte[] byteArray = stream.toByteArray();

                    FaceCrop faceCrop = new FaceCrop(this);
                    faceCrop.setFaceCropAsync(profilepic, thePic);
                    progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                    // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                    uploadDocument(byteArray, resultUri.getPath());

                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }


        } else {
            switch (requestCode) {



         /*   case Crop.REQUEST_CROP:


                    try {
                        Bundle extras = data.getExtras();

                        Bitmap thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(data));

                        FaceCrop faceCrop = new FaceCrop(this);
                        faceCrop.setFaceCropAsync(profilepic, thePic);

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thePic.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                        byte[] byteArray = stream.toByteArray();

                        progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                        // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                        uploadDocument(byteArray, Crop.getOutput(data).getPath());
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                break;
*/

                case SELECT_PICTURE:
                    if (resultCode == RESULT_OK && data != null && data.getData() != null) {

                        Uri uri = data.getData();

                        try {
                      /*  thumbnail_r = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        // profilepic.setImageBitmap(thumbnail_r);
                        //updateImage(uri);
                        FaceCrop faceCrop = new FaceCrop(this);

                        faceCrop.setFaceCropAsync(profilepic, thumbnail_r);


                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(thumbnail_r, 700, 700,
                                false);

                        // rotated
                        thumbnail_r = imageOreintationValidator(resizedBitmap,
                                uri.getPath());
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thumbnail_r.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                        // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                        uploadDocument(byteArray, uri.getPath());*/
                     /*   croppedImageUri = uri;
                        Intent cropIntent = new Intent("com.android.camera.action.CROP");
                        //indicate image type and Uri
                        cropIntent.setDataAndType(uri, "image*//*");
                        //set crop properties
                        cropIntent.putExtra("crop", "true");
                        //indicate aspect of desired crop
                        cropIntent.putExtra("aspectX", 1);
                        cropIntent.putExtra("aspectY", 1);
                        //indicate output X and Y
                        cropIntent.putExtra("outputX", 256);
                        cropIntent.putExtra("outputY", 256);
                        //retrieve data on return
                        cropIntent.putExtra("return-data", true);
                        //start the activity - we handle returning in onActivityResult
                        startActivityForResult(cropIntent, PIC_CROP);*/
                            beginCrop(data.getData());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;


                case TAKE_PICTURE:
                    if (resultCode == RESULT_OK) {

                        previewCapturedImage();

                    }

                    break;

                case PIC_CROP:

                    try {
                        Bundle extras = data.getExtras();

                        Bitmap thePic = extras.getParcelable("data");

                        FaceCrop faceCrop = new FaceCrop(this);

                        faceCrop.setFaceCropAsync(profilepic, thePic);


                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thePic.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                        byte[] byteArray = stream.toByteArray();
                        progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                        // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                        uploadDocument(byteArray, croppedImageUri.getPath());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;


            }
        }


    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        // Crop.of(source, destination).asSquare().start(this);

        CropImage.activity(source)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(16, 16)
                .setRequestedSize(1080, 1080)
                .start(this);

    }

    @SuppressLint("NewApi")
    private void previewCapturedImage() {
        try {
            // hide video preview

            //image.setVisibility(View.VISIBLE);

            // bimatp factory
           /* BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);

            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500,
                    false);

            // rotated
            thumbnail_r = imageOreintationValidator(resizedBitmap,
                    fileUri.getPath());

            //   updateImage(fileUri);


            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            thumbnail_r.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
            progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
            uploadDocument(byteArray, fileUri.getPath());

            FaceCrop faceCrop = new FaceCrop(this);
            faceCrop.setFaceCropAsync(profilepic, thumbnail_r);

            // profilepic.setImageBitmap(thumbnail_r);
            //image.setBackground(null);
            //image.setImageBitmap(thumbnail_r);
            // IsImageSet = true;*/
          /*  croppedImageUri = fileUri;
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(fileUri, "image*//*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);*/
            beginCrop(fileUri);


        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    public void updateDetails(final String Birthday) {

        if (AccessToken.getCurrentAccessToken() != null) {

            if (AccessToken.getCurrentAccessToken().getUserId() != null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {


                UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest();
                updateProfileRequest.setFb_id(AccessToken.getCurrentAccessToken().getUserId());
                updateProfileRequest.setAbout(aboutme.getText().toString().replaceAll("^\\s+", ""));
                updateProfileRequest.setBirthday(Birthday);
                updateProfileRequest.setGender("Male");
                updateProfileRequest.setLanguages(languages.getText().toString().replaceAll(", ",","));
                updateProfileRequest.setName(name.getText().toString().replaceAll("^\\s+", ""));
                updateProfileRequest.setRelationship_status(relationship.getText().toString().replaceAll("^\\s+", ""));
                updateProfileRequest.setReligion(religion.getText().toString().replaceAll("^\\s+", ""));
                updateProfileRequest.setCity(checkNull(user.getCity()));
                updateProfileRequest.setState(checkNull(user.getState()));
                updateProfileRequest.setCountry(checkNull(user.getCountry()));


                Call<SendRequestResponseModel> cm = new RequestApi().getInterface().updateProfile(updateProfileRequest);

                cm.enqueue(new Callback<SendRequestResponseModel>() {

                    @Override
                    public void onResponse(Response<SendRequestResponseModel> response) {

                        progressDialog.cancel();
                        //  Log.d("response", response.body().toString() + "");
                        if (response != null && response.body() != null && response.body().getStatus() != null && !response.body().getStatus().isEmpty() && response.body().getStatus().equalsIgnoreCase("1")) {
                            actionperformed = true;
                            user.setAbout(checkNull(aboutme.getText().toString()));
                            user.setBirthday(checkNull(Birthday));
                            user.setGender(checkNull(gender.getText().toString()));
                            user.setLanguages(checkNull(languages.getText().toString()));
                            user.setName(checkNull(name.getText().toString()));
                            user.setRelationship_status(checkNull(relationship.getText().toString()));
                            user.setReligion(checkNull(religion.getText().toString()));

                            Toast.makeText(UserProfileEdit.this, "Details Updated", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        } else {
                            actionperformed = false;
                            Toast.makeText(UserProfileEdit.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Toast.makeText(UserProfileEdit.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                        progressDialog.cancel();
                    }


                });
            }
        }

    }

    public void updateImage(Uri uri) {


        Map<String, RequestBody> map = new HashMap<>();
        File file = new File(uri.getPath());

        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);

        Call<com.squareup.okhttp.Response> call = new RequestApi().getInterface().upload("token", map);
        call.enqueue(new Callback<com.squareup.okhttp.Response>() {
            @Override
            public void onResponse(Response<com.squareup.okhttp.Response> response) {

                Log.d("Res", response.toString());
            }

            @Override
            public void onFailure(Throwable t) {

                t.printStackTrace();
            }


        });



      /*  File file = new File(uri.getPath());



        RequestBody photo = RequestBody.create(MediaType.parse("application/image"), file);
        RequestBody body = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addFormDataPart("photo", file.getName(), photo)
                .build();

        Call<com.squareup.okhttp.Response> call = new RequestApi().getInterface().changeUserPhoto("test", body);

        call.enqueue(new Callback<com.squareup.okhttp.Response>() {
            @Override
            public void onResponse(Response<com.squareup.okhttp.Response> response) {

                Log.v("Upload", "success");
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

       RequestBody sort = RequestBody.create(MediaType.parse("text/plain"), "test upload");

        RequestBody requestBody =
                RequestBody.create(MediaType.parse("image/*"), file);

        Call<Void> call = new  RequestApi().getInterface().upload(requestBody, sort);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Response<Void> response) {
                Log.v("Upload", "success");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Upload", t.getMessage());
            }
        });*/

    }


    public void uploadDocument(final byte[] bytes, final String uri) {

        RequestQueue requestQueue = Volley.newRequestQueue(UserProfileEdit.this);

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, SERVER_URL, new com.android.volley.Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {


                String resultResponse = new String(response.data);

                JSONObject jsonResponse = null;

                try {
                    Log.d("Response",resultResponse+"\t"+SERVER_URL);
                    jsonResponse = new JSONObject(resultResponse);

                    if (jsonResponse != null && jsonResponse.has("url") && !jsonResponse.getString("url").isEmpty()) {

                        actionperformed = true;
                        user.setProfile_url(jsonResponse.getString("url"));
                        Toast.makeText(UserProfileEdit.this, "Profile pic updated", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(UserProfileEdit.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.cancel();
                    //   chooseImage.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_menu_camera));

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(UserProfileEdit.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                    progressDialog.cancel();
                    // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                }


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                Toast.makeText(UserProfileEdit.this, "Unable to connect server", Toast.LENGTH_SHORT).show();
                progressDialog.cancel();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("fb_id", AccessToken.getCurrentAccessToken().getUserId() + "");

                return params;
            }

            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("uploaded_file", new VolleyMultipartRequest.DataPart(uri, bytes, "image/jpeg"));


                return params;
            }

        };

        requestQueue.add(multipartRequest);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


    }

    public void onErrorResponse(VolleyError error) {

        // As of f605da3 the following should work
        NetworkResponse response = error.networkResponse;
        if (error instanceof ServerError && response != null) {
            try {
                String res = new String(response.data,
                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                // Now you can use any deserializer to make sense of data
                JSONObject obj = new JSONObject(res);
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                e1.printStackTrace();
            } catch (JSONException e2) {
                // returned data is not JSONObject?
                e2.printStackTrace();
            }
        }
    }

    // for roted image......
    private Bitmap imageOreintationValidator(Bitmap bitmap, String path) {

        ExifInterface ei;
        try {
            ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }


    private Bitmap rotateImage(Bitmap source, float angle) {

        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                    source.getHeight(), matrix, true);
        } catch (OutOfMemoryError err) {
            source.recycle();
            Date d = new Date();
            CharSequence s = DateFormat
                    .format("MM-dd-yy-hh-mm-ss", d.getTime());
            String fullPath = Environment.getExternalStorageDirectory()
                    + "/RYB_pic/" + s.toString() + ".jpg";
            if ((fullPath != null) && (new File(fullPath).exists())) {
                new File(fullPath).delete();
            }
            bitmap = null;
            err.printStackTrace();
        }
        return bitmap;
    }


    public static Bitmap decodeSampledBitmapFromResource(String pathToFile,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathToFile, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        Log.e("inSampleSize", "inSampleSize______________in storage"
                + options.inSampleSize);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathToFile, options);
    }


    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

        }

        return inSampleSize;
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }

    public void startImageSelection() {
        Intent intent = new Intent();

        intent.setType("image/*");


        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGES);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {


            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //reload my activity with permission granted or use the features what required the permission
                    // selectImage();

                } else {
                    //   Toast.makeText(myAccountActivity.this, "Please allow us to Take picture from your device", Toast.LENGTH_LONG).show();


                    Toast.makeText(UserProfileEdit.this, "Please allow us to Take picture from your device", Toast.LENGTH_SHORT).show();

                }
            }
            break;

            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    if (checkPlayServices()) {

                        // Building the GoogleApi client
                        customProgressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                        buildGoogleApiClient();
                    }

                } else {

                    Toast.makeText(UserProfileEdit.this, "Please provide us the required permission to auto fill your address", Toast.LENGTH_SHORT).show();
                }

        }

    }


    /**
     * Method to display the location on UI
     */
    private boolean displayLocation() {


        if (ActivityCompat.checkSelfPermission(UserProfileEdit.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UserProfileEdit.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(UserProfileEdit.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);

            customProgressDialog.cancel();
            return false;
        }


        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {

            try {

                if (mGoogleApiClient != null) {
                    mGoogleApiClient.disconnect();
                }

                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();
                Geocoder gcd = new Geocoder(UserProfileEdit.this, Locale.getDefault());
                List<android.location.Address> addresses = null;
                addresses = gcd.getFromLocation(latitude, longitude, 1);
                if (addresses != null && addresses.size() > 0) {
                    customProgressDialog.cancel();
                    city.setText("  " + addresses.get(0).getLocality());
                    user.setCity("" + addresses.get(0).getLocality());
                    user.setState("" + addresses.get(0).getAdminArea());
                    user.setCountry("" + addresses.get(0).getCountryName());
                } else {
                    Toast.makeText(UserProfileEdit.this, "Unable to get your city", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            customProgressDialog.cancel();
            Toast.makeText(UserProfileEdit.this, "Couldn't get the location. Make sure location is enabled on the device", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();

        checkPlayServices();
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        displayLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }
}
