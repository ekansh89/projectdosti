package com.tipstat.dosti.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tipstat.dosti.Fragments.LocationPrefernceFragment;
import com.tipstat.dosti.Models.LocationModel;
import com.tipstat.dosti.Models.SendRequestResponseModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.SetPreferncesRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.ChipView.ChipsView;
import com.tipstat.dosti.Views.ChipView.Contact;
import com.tipstat.dosti.Views.CustomProgressDialog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.apptik.widget.MultiSlider;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class SetPreferencesActivity extends AppCompatActivity {


    private MultiSlider multiSlider;
    private TextView agemin, agemax, locationtitle, nolocation, countrytitle, india, pakistan;
    private com.tipstat.dosti.Views.ChipView.ChipsView mChipsView;
    private Button save;
    private List<LocationModel> locationdata;
    private CheckBox male, female;
    private CustomProgressDialog progressDialog;
    private LinearLayout countrylinearlayout;
    private boolean actionPerformed = false, indiaboolean = false, pakistanboolean = false;
    private List<String> locations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_preferences);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(" ");
        // getSupportActionBar().setIcon(R.mipmap.alvoff_logo_wb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        multiSlider = (MultiSlider) findViewById(R.id.range_slider);
        agemin = (TextView) findViewById(R.id.agemin);
        agemax = (TextView) findViewById(R.id.agemax);
        locationtitle = (TextView) findViewById(R.id.locationtitle);
        save = (Button) findViewById(R.id.save);
        mChipsView = (ChipsView) findViewById(R.id.cv_contacts);
        mChipsView.getEditText().setCursorVisible(false);
        countrytitle = (TextView) findViewById(R.id.countrytitle);
        countrylinearlayout = (LinearLayout) findViewById(R.id.linearlayoutcountry);
        india = (TextView) findViewById(R.id.india);
        pakistan = (TextView) findViewById(R.id.pakistan);
        male = (CheckBox) findViewById(R.id.male);

        female = (CheckBox) findViewById(R.id.female);
        nolocation = (TextView) findViewById(R.id.nolocation);
        multiSlider.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {

                agemin.setText(multiSlider.getThumb(0).getValue() + "");
                agemax.setText(multiSlider.getThumb(1).getValue() + "");
            }
        });

        multiSlider.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Log.d("touch", String.valueOf(event.getX()) + "x" + String.valueOf(event.getY()));
                return false;
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        locationtitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //updating selected locations
                List<ChipsView.Chip> chips = mChipsView.getChips();
                List<String> location_Temp = new ArrayList<String>();
                if (chips != null && chips.size() > 0) {
                    for (int i = 0; i < chips.size(); i++) {
                        location_Temp.add(chips.get(i).getContact().getEmailAddress());
                    }

                    if (location_Temp != null && location_Temp.size() > 0 && locations != null && locations.size() > 0) {
                        for (int i = 0; i < locations.size(); i++) {
                            if (!location_Temp.contains(locations.get(i))) {
                                locations.remove(i);
                            }
                        }
                    }

                } else {
                    if (locations != null)
                        locations.clear();
                }


                final LocationPrefernceFragment myBottomSheet = LocationPrefernceFragment.newInstance("Modal Bottom Sheet", locations);
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

            }
        });

        nolocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationtitle.performClick();
            }
        });

        // getData();
        setPrefernces();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String gender = "";

                if (male.isChecked() && female.isChecked()) {
                    gender = "both";
                } else if (male.isChecked() && !female.isChecked()) {
                    gender = "male";
                } else if (!male.isChecked() && female.isChecked()) {
                    gender = "female";
                } else if (!male.isChecked() && !female.isChecked()) {
                    gender = "both";
                }

                SetPreferncesRequest setPreferncesRequest = new SetPreferncesRequest();
                setPreferncesRequest.setGender(gender);
                setPreferncesRequest.setFb_id(AccessToken.getCurrentAccessToken().getUserId());
                setPreferncesRequest.setMinage(multiSlider.getThumb(0).getValue() + "");
                setPreferncesRequest.setMaxage(multiSlider.getThumb(1).getValue() + "");

                List<String> locations = new ArrayList<String>();
                List<String> locations_fullname = new ArrayList<String>();
                List<ChipsView.Chip> chips = mChipsView.getChips();

                for (int i = 0; i < chips.size(); i++) {
                    locations_fullname.add(chips.get(i).getContact().getEmailAddress().replace(',', '-'));
                    locations.add(chips.get(i).getContact().getEmailAddress().replace(',', '-'));
                   /* String[] temp = chips.get(i).getContact().getEmailAddress().split(",");
                    if (temp != null && temp.length > 0) {

                        if (temp.length == 2)
                            locations.add(temp[1]);
                        else if (temp.length == 1)
                            locations.add(temp[0]);
                    }*/
                }

              /*  if (locationdata != null) {
                    for (int i = 0; i < locationdata.size(); i++) {


                        locations.add(locationdata.get(i).getState());
                    }
                }*/

                setPreferncesRequest.setLocations(locations);

                Gson gson = new Gson();
                Log.d("location", gson.toJson(setPreferncesRequest) + "");

                if (indiaboolean && !pakistanboolean) {
                    setPreferncesRequest.setCountry("India");
                    savePrefernces(setPreferncesRequest, locations_fullname);
                } else if (!indiaboolean && pakistanboolean) {
                    setPreferncesRequest.setCountry("Pakistan");
                    savePrefernces(setPreferncesRequest, locations_fullname);
                } else {
                    Toast.makeText(SetPreferencesActivity.this, "Please select a country", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }


    public void setPrefernces() {

        Intent intent = getIntent();


        india.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                indiaboolean = true;
                pakistanboolean = false;
                india.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                india.setTextColor(getResources().getColor(android.R.color.white));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    pakistan.setBackground(getResources().getDrawable(R.drawable.black_border_white));
                    pakistan.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                } else {
                    pakistan.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_white));
                    pakistan.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                }

            }
        });
        pakistan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                pakistanboolean = true;
                indiaboolean = false;
                pakistan.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                pakistan.setTextColor(getResources().getColor(android.R.color.white));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    india.setBackground(getResources().getDrawable(R.drawable.black_border_white));
                    india.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                } else {
                    india.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_white));
                    india.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                }

            }
        });


        SharedPreferences pref = getSharedPreferences(Config.SHARED_PREF, 0);
        String userpreferences = pref.getString("userpreferences", null);
        String userCountry = pref.getString("usercountry", null);
        if (userpreferences != null && !userpreferences.isEmpty()) {
            Gson gson = new Gson();
            SetPreferncesRequest setPreferncesRequests = gson.fromJson(userpreferences, SetPreferncesRequest.class);
            if (setPreferncesRequests != null) {
                SetPreferncesRequest temp = setPreferncesRequests;
                if (temp != null) {
                    if (temp.getGender() != null) {
                        if (temp.getGender().equalsIgnoreCase("both")) {
                            male.setChecked(true);
                            female.setChecked(true);
                        } else if (temp.getGender().equalsIgnoreCase("male")) {
                            male.setChecked(true);
                            female.setChecked(false);
                        } else if (temp.getGender().equalsIgnoreCase("female")) {
                            female.setChecked(true);
                            male.setChecked(false);
                        } else {
                            male.setChecked(true);
                            female.setChecked(true);
                        }
                    } else {
                        male.setChecked(true);
                        female.setChecked(true);
                    }

                    if (temp.getMinage() != null && !temp.getMinage().isEmpty()) {
                        multiSlider.getThumb(0).setValue(Integer.parseInt(temp.getMinage()));
                    } else {
                        multiSlider.getThumb(0).setValue(0);
                    }


                    if (temp.getMaxage() != null && !temp.getMaxage().isEmpty()) {
                        multiSlider.getThumb(1).setValue(Integer.parseInt(temp.getMaxage()));
                    } else {
                        multiSlider.getThumb(0).setValue(100);
                    }

                    if (temp.getLocations() != null && temp.getLocations().size() > 0) {

                        locations = temp.getLocations();
                        nolocation.setVisibility(View.GONE);
                        for (int i = 0; i < temp.getLocations().size(); i++) {

                            final String email = temp.getLocations().get(i).replace("-", ", ");
                            temp.getLocations().set(i,email);
                            final Uri imgUrl = Math.random() > .7d ? null : Uri.parse("https://robohash.org/" + Math.abs(email.hashCode()));
                            final Contact contact = new Contact(null, null, null, email, null);

                            Handler refresh = new Handler(Looper.getMainLooper());
                            refresh.post(new Runnable() {
                                public void run() {
                                    mChipsView.addChip(email, "", contact);
                                }
                            });


                        }

                    } else {
                        nolocation.setVisibility(View.VISIBLE);
                    }

                    if (userCountry != null && !userCountry.isEmpty() && (userCountry.equalsIgnoreCase("india") || userCountry.equalsIgnoreCase("pakistan"))) {
                        locationtitle.setVisibility(View.VISIBLE);
                        countrylinearlayout.setVisibility(View.GONE);
                        countrytitle.setVisibility(View.GONE);
                        if (userCountry.equalsIgnoreCase("india")) {
                            pakistanboolean = true;
                        } else if (userCountry.equalsIgnoreCase("pakistan")) {
                            indiaboolean = true;
                        }
                    } else {
                        countrylinearlayout.setVisibility(View.VISIBLE);
                        countrytitle.setVisibility(View.VISIBLE);
                        if (temp.getCountry() != null && !temp.getCountry().isEmpty()) {

                            if (temp.getCountry().equalsIgnoreCase("india")) {
                                locationtitle.setVisibility(View.VISIBLE);
                                indiaboolean = true;
                                india.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                india.setTextColor(getResources().getColor(android.R.color.white));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    pakistan.setBackground(getResources().getDrawable(R.drawable.black_border_white));
                                    pakistan.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                                } else {
                                    pakistan.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_white));
                                    pakistan.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                                }

                            } else if (temp.getCountry().equalsIgnoreCase("pakistan")) {
                                locationtitle.setVisibility(View.VISIBLE);
                                pakistanboolean = true;
                                pakistan.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                pakistan.setTextColor(getResources().getColor(android.R.color.white));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    india.setBackground(getResources().getDrawable(R.drawable.black_border_white));
                                    india.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                                } else {
                                    india.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_white));
                                    india.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                                }
                            } else {
                                locationtitle.setVisibility(View.GONE);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    india.setBackground(getResources().getDrawable(R.drawable.black_border_white));
                                    india.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                                } else {
                                    india.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_white));
                                    india.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                                }
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    pakistan.setBackground(getResources().getDrawable(R.drawable.black_border_white));
                                    pakistan.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                                } else {
                                    pakistan.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_white));
                                    pakistan.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                                }
                            }
                        } else {
                            locationtitle.setVisibility(View.VISIBLE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                india.setBackground(getResources().getDrawable(R.drawable.black_border_white));
                                india.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                            } else {
                                india.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_white));
                                india.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                pakistan.setBackground(getResources().getDrawable(R.drawable.black_border_white));
                                pakistan.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                            } else {
                                pakistan.setBackgroundDrawable(getResources().getDrawable(R.drawable.black_border_white));
                                pakistan.setTextColor(getResources().getColor(R.color.mediumlightgrey));
                            }

                        }
                    }


                } else {

                    male.setChecked(true);
                    female.setChecked(true);
                    multiSlider.getThumb(0).setValue(0);
                    multiSlider.getThumb(1).setValue(100);
                    if (userCountry != null && !userCountry.isEmpty()) {
                        locationtitle.setVisibility(View.VISIBLE);
                        countrylinearlayout.setVisibility(View.GONE);
                        countrytitle.setVisibility(View.GONE);
                        if (userCountry.equalsIgnoreCase("india")) {
                            pakistanboolean = true;
                        } else if (userCountry.equalsIgnoreCase("pakistan")) {
                            indiaboolean = true;
                        } else {
                            locationtitle.setVisibility(View.GONE);
                            countrylinearlayout.setVisibility(View.VISIBLE);
                            countrytitle.setVisibility(View.VISIBLE);
                        }

                    } else {

                        countrylinearlayout.setVisibility(View.VISIBLE);
                        countrytitle.setVisibility(View.VISIBLE);
                        locationtitle.setVisibility(View.GONE);
                    }
                }


            } else {

                male.setChecked(true);
                female.setChecked(true);
                multiSlider.getThumb(0).setValue(0);
                multiSlider.getThumb(1).setValue(100);

                if (userCountry != null && !userCountry.isEmpty()) {
                    locationtitle.setVisibility(View.VISIBLE);

                    if (userCountry.equalsIgnoreCase("india")) {
                        pakistanboolean = true;
                        countrylinearlayout.setVisibility(View.GONE);
                        countrytitle.setVisibility(View.GONE);
                    } else if (userCountry.equalsIgnoreCase("pakistan")) {
                        indiaboolean = true;
                        countrylinearlayout.setVisibility(View.GONE);
                        countrytitle.setVisibility(View.GONE);
                    } else {
                        locationtitle.setVisibility(View.GONE);
                        countrylinearlayout.setVisibility(View.VISIBLE);
                        countrytitle.setVisibility(View.VISIBLE);
                    }
                } else {

                    countrylinearlayout.setVisibility(View.VISIBLE);
                    countrytitle.setVisibility(View.VISIBLE);
                    locationtitle.setVisibility(View.GONE);
                }
            }


        } else {

            male.setChecked(true);
            female.setChecked(true);
            multiSlider.getThumb(0).setValue(0);
            multiSlider.getThumb(1).setValue(100);

            if (userCountry != null && userCountry.isEmpty()) {
                locationtitle.setVisibility(View.VISIBLE);
                countrylinearlayout.setVisibility(View.GONE);
                countrytitle.setVisibility(View.GONE);
                if (userCountry.equalsIgnoreCase("india")) {
                    pakistanboolean = true;
                } else if (userCountry.equalsIgnoreCase("pakistan")) {
                    indiaboolean = true;
                } else {
                    locationtitle.setVisibility(View.GONE);
                    countrylinearlayout.setVisibility(View.VISIBLE);
                    countrytitle.setVisibility(View.VISIBLE);
                }

            } else {
                countrylinearlayout.setVisibility(View.VISIBLE);
                countrytitle.setVisibility(View.VISIBLE);
                locationtitle.setVisibility(View.GONE);
            }
        }


    }

    @Override
    public void onBackPressed() {
        if (actionPerformed) {
            Intent intent = new Intent();
            setResult(526, intent);
            finish();//finishing activity
        } else {

            super.onBackPressed();
        }
    }

    public void savePrefernces(final SetPreferncesRequest setPreferncesRequest, final List<String> locations_fullname) {
        if (AccessToken.getCurrentAccessToken() != null) {

            if (AccessToken.getCurrentAccessToken().getUserId() != null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {

                progressDialog = CustomProgressDialog.show(SetPreferencesActivity.this);
                Call<SendRequestResponseModel> cm = new RequestApi().getInterface().savepreferences(setPreferncesRequest);

                cm.enqueue(new Callback<SendRequestResponseModel>() {
                    @Override
                    public void onResponse(Response<SendRequestResponseModel> response) {

                        if (response != null && response.body() != null && response.body().getStatus() != null && response.body().getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(SetPreferencesActivity.this, "Preferences Saved", Toast.LENGTH_SHORT).show();
                            progressDialog.cancel();
                            actionPerformed = true;
                            onBackPressed();
                           /* List<ChipsView.Chip> chips = mChipsView.getChips();
                            List<String> locations = new ArrayList<String>();
                            for (int i = 0; i < chips.size(); i++) {
                                String[] temp = chips.get(i).getContact().getEmailAddress().split(",");
                                if (temp != null && temp.length > 0) {
                                    if (temp.length == 2) {
                                        locations.add(temp[1]);
                                    } else if (temp.length == 1) {
                                        locations.add(temp[0]);
                                    }
                                }

                            }*/
                            setPreferncesRequest.setLocations(locations_fullname);
                            Gson gson = new Gson();
                            SharedPreferences pref = getSharedPreferences(Config.SHARED_PREF, 0);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("userpreferences", gson.toJson(setPreferncesRequest));
                            editor.commit();

                            Intent intent = getIntent();
                            if (intent != null && intent.hasExtra("nocoutry")) {

                                startActivity(new Intent(SetPreferencesActivity.this, MainActivity.class));
                                finish();
                            }

                        } else {
                            Toast.makeText(SetPreferencesActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        progressDialog.cancel();
                        Toast.makeText(SetPreferencesActivity.this, "Unable to connect server", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        } else {
            progressDialog.cancel();
        }
    }

    public void setLocations(List<LocationModel> data) {


        locationdata = data;

        if (data != null && data.size() > 0) {

            nolocation.setVisibility(View.GONE);
            locations = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {

                LocationModel locationModel = data.get(i);

                if (locationModel.isSelected() == true) {
                    locations.add(data.get(i).getCity() + ", " + data.get(i).getState());
                    if (!hasChip(locationModel.getCity() + ", " + locationModel.getState())) {
                        final String email = data.get(i).getCity() + ", " + data.get(i).getState();
                        final Uri imgUrl = Math.random() > .7d ? null : Uri.parse("https://robohash.org/" + Math.abs(email.hashCode()));
                        final Contact contact = new Contact(null, null, null, email, null);

                        Handler refresh = new Handler(Looper.getMainLooper());
                        refresh.post(new Runnable() {
                            public void run() {
                                mChipsView.addChip(email, "", contact);
                                locations.add(email);


                            }
                        });
                    }

                } else {
                    if (locationModel.isSelected() == false && hasChip(locationModel.getCity() + ", " + locationModel.getState())) {
                        List<ChipsView.Chip> chips = mChipsView.getChips();
                        for (int j = 0; j < chips.size(); j++) {
                            Contact contact = chips.get(j).getContact();
                            if (contact != null) {
                                if (contact.getEmailAddress().equalsIgnoreCase(locationModel.getCity() + ", " + locationModel.getState())) {
                                    mChipsView.removeChipBy(contact);
                                }
                            }
                        }
                    }
                }

            }
        } else {
            nolocation.setVisibility(View.VISIBLE);
        }


    }

    public boolean hasChip(String location) {
        List<ChipsView.Chip> chips = mChipsView.getChips();
        if (chips != null && chips.size() > 0) {
            for (int i = 0; i < chips.size(); i++) {
                if (chips.get(i).getContact().getEmailAddress().equalsIgnoreCase(location)) {
                    return true;

                }

            }
            return false;
        } else {
            return false;
        }


    }


}
