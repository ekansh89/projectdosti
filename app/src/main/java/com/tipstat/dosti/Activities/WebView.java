package com.tipstat.dosti.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tipstat.dosti.R;
import com.tipstat.dosti.Views.ErrorView.ErrorView;


public class webview extends AppCompatActivity {

    WebView webView;
    TextView toolbar_title;
    ImageView toolbar_close;
    Button share;
    ProgressBar progressBar;
    private ErrorView errorView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Log.e("Alert","Lets See if it Works !!!");
                paramThrowable.printStackTrace();
            }
        });

        final Intent i  = getIntent();
        webView = (WebView) findViewById(R.id.webView1);
        errorView = (ErrorView) findViewById(R.id.errorview);

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        toolbar_close = (ImageView) findViewById(R.id.toolbar_close);
        toolbar_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        if(i!=null)
        {
            if(i.getStringExtra("url")!=null && !i.getStringExtra("url").isEmpty())
            {
                webView.loadUrl(i.getStringExtra("url"));
            }
            else
                webView.loadUrl("http://www.google.com");
        }


        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                toolbar_title.setText(view.getTitle());
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                view.setVisibility(View.GONE);
                view.loadUrl("about:blank");
                webView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                view.setVisibility(View.GONE);
                webView.setVisibility(View.GONE);
                view.loadUrl("about:blank");
                errorView.setVisibility(View.VISIBLE);

            }
        });

        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                if(i.getStringExtra("url")!=null && !i.getStringExtra("url").isEmpty())
                {
                    webView.loadUrl(i.getStringExtra("url"));
                }
                else
                    webView.loadUrl("http://www.facebook.com");

                webView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
            }
        });


        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (!TextUtils.isEmpty(title)) {
                    {
                        if(title.equalsIgnoreCase("about:blank"))
                        {
                            toolbar_title.setText("Alvoff");
                        }
                        else
                            toolbar_title.setText(title);
                    }
                }
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                progressBar.setProgress(newProgress);
            }
        });


    }

}
