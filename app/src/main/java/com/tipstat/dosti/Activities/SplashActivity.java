package com.tipstat.dosti.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.tipstat.dosti.Adapters.SplashScreenViewPagerAdapter;
import com.tipstat.dosti.Models.LoginModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.LoginApiRequest;
import com.tipstat.dosti.Retrofit.ApiClient;
import com.tipstat.dosti.Retrofit.ApiInterface;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.CustomProgressDialog;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


public class SplashActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private ViewPager viewPager;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private SplashScreenViewPagerAdapter splashScreenViewPagerAdapter;
    private LoginButton facebooklogin;
    private CustomProgressDialog progressDialog;
    private List<String> likes;
    private CallbackManager callbackManager;

    private RelativeLayout mainLayout;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(SplashActivity.this.getApplicationContext());
        setContentView(R.layout.activity_splash);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        printKeyHash(SplashActivity.this);
        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {


            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {

            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        mainLayout = (RelativeLayout) findViewById(R.id.content_splash);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        facebooklogin = (LoginButton) findViewById(R.id.facebooklogin);
        splashScreenViewPagerAdapter = new SplashScreenViewPagerAdapter(this);
        viewPager.setAdapter(splashScreenViewPagerAdapter);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        viewPager.setOnPageChangeListener(this);
        setUiPageViewController();
        facebooklogin.setReadPermissions("public_profile", "email", "user_birthday", "user_friends", "read_custom_friendlists", "user_about_me", "user_hometown", "user_likes", "user_location", "user_relationship_details", "user_work_history");
        facebooklogin.registerCallback(callbackManager, callback);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


    }

    public void getLikes() {

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday,first_name,middle_name,last_name,hometown,work");


        GraphRequest graphRequest = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + AccessToken.getCurrentAccessToken().getUserId(),
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            /* handle the result */
                        Log.d("details", response.toString());
                    }
                }
        );
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();


    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (android.content.pm.Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isLoggedIn()) {
            // getData();
          getData();

        }
        else
        {
            mainLayout.setVisibility(View.VISIBLE);
        }
    }


    public void getMutualLikes() {
        Bundle params = new Bundle();
        params.putString("fields", "context.fields(mutual_likes)");
/* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + AccessToken.getCurrentAccessToken().getUserId(),
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            /* handle the result */
                        Log.d("mutuallikes", response.toString() + "");
                    }
                }
        ).executeAsync();

    }

    public void getData() {


        progressDialog = CustomProgressDialog.show(SplashActivity.this);


        final SharedPreferences pref = getSharedPreferences(Config.SHARED_PREF, 0);

       /* TelephonyManager teleMgr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String countryISOCode = "";
        if (teleMgr != null) {
            countryISOCode = teleMgr.getNetworkCountryIso();

            countryISOCode = new Locale("", countryISOCode).getDisplayCountry();
            Log.d("country name",countryISOCode+"");

/*            if (teleMgr.getNetworkCountryIso().equalsIgnoreCase("in"))
                countryISOCode = "india";

            else  if (teleMgr.getNetworkCountryIso().equalsIgnoreCase("PAK"))
            {
                countryISOCode = "pakistan";
            }
            else
                countryISOCode = "pakistan";
                 }

        Log.d("country", teleMgr.getNetworkCountryIso() + "," + teleMgr.getNetworkCountryIso());
                */

        LoginApiRequest loginApiRequest = new LoginApiRequest();
        loginApiRequest.setAccess_token(AccessToken.getCurrentAccessToken().getToken() + "");
        loginApiRequest.setFb_id(AccessToken.getCurrentAccessToken().getUserId() + "");
        loginApiRequest.setFcm_id(pref.getString("regId", null));
        // loginApiRequest.setCountry(countryISOCode);

        final Call<LoginModel> cm = new RequestApi().getInterface().login(loginApiRequest);

        cm.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Response<LoginModel> response) {

              /*  try {

                    if (response != null && response.body() != null)

                    {
                        progressDialog.cancel();

                        likes = Arrays.asList(response.body().getLikes().split(",")); ;
                        final LoginModel user = response.body().getUser();

                        if (user != null && user.getCountry() != null && !user.getCountry().isEmpty() && (user.getCountry().equalsIgnoreCase("india") || user.getCountry().equalsIgnoreCase("pakistan") || user.getCountry().equalsIgnoreCase("pakisthan"))) {

                            Gson gson = new Gson();
                            SharedPreferences pref = getSharedPreferences(Config.SHARED_PREF, 0);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("username", user.getName());
                            editor.putString("usercountry", user.getCountry() + "");
                            if (user.getPreflocations() != null && user.getPreflocations().getCountry() != null)
                                editor.putString("userpreferedcountry", user.getPreflocations().getCountry());
                            editor.putString("userpreferences", gson.toJson(response.body().getPreflocations()));
                            editor.commit();
                            Log.d("preferences", gson.toJson(response.body().getPreflocations()));
                        } else {

                            if (response.body().getPreflocations() != null && response.body().getPreflocations().getCountry() != null && !response.body().getPreflocations().getCountry().isEmpty()) {

                                Gson gson = new Gson();
                                SharedPreferences pref = getSharedPreferences(Config.SHARED_PREF, 0);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("username", user.getName());
                                editor.putString("usercountry", user.getCountry() + "");
                                if (user.getPreflocations() != null && user.getPreflocations().getCountry() != null)
                                    editor.putString("userpreferedcountry", user.getPreflocations().getCountry());
                                editor.putString("userpreferences", gson.toJson(response.body().getPreflocations()));
                                editor.commit();
                                Log.d("preferences", gson.toJson(response.body().getPreflocations()));
                            } else {
                                startActivity(new Intent(SplashActivity.this, SetPreferencesActivity.class).putExtra("nocoutry", true));
                                finish();
                            }

                            Intent i = new Intent(SplashActivity.this, MainActivity.class);
                            Profile profile = Profile.getCurrentProfile();
                            if (profile != null)
                                i.putExtra("name", profile.getName() + "");
                            startActivity(i);
                            finish();
                        }
                    } else {
                        Toast.makeText(SplashActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        progressDialog.cancel();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.cancel();


                }
*/
                progressDialog.cancel();
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                Profile profile = Profile.getCurrentProfile();
                if (profile != null)
                    i.putExtra("name", profile.getName() + "");
                startActivity(i);
                finish();
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.cancel();

                Log.d("failure", t.getLocalizedMessage() + "");
                Toast.makeText(SplashActivity.this,"Something went wrong, try again",Toast.LENGTH_SHORT).show();
                LoginManager.getInstance().logOut();
            }


        });


    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            Log.d("Accesstoken", accessToken.getToken() + "\t" + accessToken.getUserId());

        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    private void setUiPageViewController() {

        dotsCount = splashScreenViewPagerAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.viewpagernonselecteditemdot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(10, 0, 10, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.viewpagerselecteditemdot));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.viewpagernonselecteditemdot));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.viewpagerselecteditemdot));


    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
