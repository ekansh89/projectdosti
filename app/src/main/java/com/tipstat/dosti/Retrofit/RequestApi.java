package com.tipstat.dosti.Retrofit;

/**
 * Created by nikhi on 10/21/2016.
 */

public class RequestApi extends ApiClient {

    private ApiInterface mInterface;

    public RequestApi() {

        mInterface = mRetrofit.create(ApiInterface.class);
    }

    public ApiInterface getInterface() {
        return mInterface;
    }
}