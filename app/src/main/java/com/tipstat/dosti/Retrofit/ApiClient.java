package com.tipstat.dosti.Retrofit;

/**
 * Created by nikhi on 10/21/2016.
 */

import android.util.Log;



import okio.Buffer;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.ResponseBody;
import com.tipstat.dosti.Utils.Config;


import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * Daniele A - 8/27/15.
 */
public class ApiClient {

    protected Retrofit mRetrofit;

    public ApiClient() {

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        OkHttpClient httpClient = new OkHttpClient();



      httpClient.interceptors().add(new Interceptor() {
            @Override
            public com.squareup.okhttp.Response intercept(
                    Chain chain) throws IOException {

                 Request original = chain.request();
                 Request request = original.newBuilder()
                      .header("Content-type", "application/json; charset=utf-8")

                        .method(original.method(), original.body()).build();
              com.squareup.okhttp.Response response = chain.proceed(request);

                Log.d("Url : ", request.url().toString());

                if (request.body() != null) {

                    Buffer buffer = new Buffer();
                    request.body().writeTo(buffer);
                    String body = buffer.readUtf8();
                    Log.d("body : ", body);
                }

                String strBody = response.body().string();
                Log.d("Response", strBody);
                return response
                        .newBuilder()
                        .body(ResponseBody.create(
                               response.body().contentType(), strBody)).build();

            }
        });


        mRetrofit = new Retrofit.Builder().baseUrl(Config.BaseUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }



}
