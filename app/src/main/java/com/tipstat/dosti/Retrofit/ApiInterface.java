package com.tipstat.dosti.Retrofit;

/**
 * Created by nikhi on 10/21/2016.
 */


import com.squareup.okhttp.MultipartBuilder;

import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.tipstat.dosti.Models.LocationModel;
import com.tipstat.dosti.Models.LoginModel;
import com.tipstat.dosti.Models.MainActivityAlertFragmentModel;
import com.tipstat.dosti.Models.MainActivityFriendsListModel;
import com.tipstat.dosti.Models.MainActivityListFragmentModel;
import com.tipstat.dosti.Models.SendRequestResponseModel;
import com.tipstat.dosti.Models.UserAttributesModel;
import com.tipstat.dosti.Models.UsersListModel;
import com.tipstat.dosti.RequestModels.ExpressInterestRequest;
import com.tipstat.dosti.RequestModels.FcmIdRequest;
import com.tipstat.dosti.RequestModels.FetchNotificationsRequest;
import com.tipstat.dosti.RequestModels.LoginApiRequest;
import com.tipstat.dosti.RequestModels.MutualFriendsRequest;
import com.tipstat.dosti.RequestModels.SetPreferncesRequest;
import com.tipstat.dosti.RequestModels.UpdateProfileRequest;

import java.util.Map;


import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.PartMap;
import retrofit.http.Path;
import retrofit.http.QueryMap;

import static com.facebook.HttpMethod.POST;


public interface ApiInterface {


    @retrofit.http.POST("dosti/v1/login")
    Call<LoginModel> login(@Body LoginApiRequest query);

    @POST("dosti/v1/fcmRegistration")
    Call<FcmIdRequest> fcmregistartion(@Body FcmIdRequest query);

    @POST("dosti/v1/expressInterest")
    Call<SendRequestResponseModel> expressinterest(@Body ExpressInterestRequest query);

    @POST("dosti/v1/acceptInterest")
    Call<ExpressInterestRequest> acceptInterest(@Body ExpressInterestRequest query);

    @GET("dosti/v1/fetchNotifications")
    Call<MainActivityAlertFragmentModel> fetchNotifications(@QueryMap Map<String, String> params);

    @GET("dosti/v1/getUserInfo")
    Call<LoginModel> getUserInfo(@QueryMap Map<String, String> params);

    @GET("dosti/v1/getPreferredUserList")
    Call<MainActivityListFragmentModel> getPreferedUsersList(@QueryMap Map<String, String> params);

    @POST("dosti/v1/updateProfile")
    Call<SendRequestResponseModel> updateProfile(@Body UpdateProfileRequest query);

    @GET("dosti/v1/getLocationPreference")
    Call<LocationModel> getLocationPreference(@QueryMap Map<String, String> params);

    @GET("dosti/v1/mutualFriends")
    Call<MainActivityFriendsListModel> mutualFriends(@QueryMap Map<String, String> params);

    @GET("dosti/v1/user_attributes")
    Call<UserAttributesModel> user_attributes(@QueryMap Map<String, String> params);

    @POST("dosti/v1/savepreferences")
    Call<SendRequestResponseModel> savepreferences (@Body SetPreferncesRequest query);

    @Multipart
    @POST("dosti/v1/uploadImage")
    Call<ResponseBody> postImage(Part image, @Part("name") RequestBody name);


    @Multipart
    @POST("dosti/v1/uploadImage")
    Call<ResponseBody> upload(@Part("description") RequestBody description,
                              @PartMap Part file);

    @Multipart
    @POST("dosti/v1/uploadImage")
    Call<Response> upload(
            @Header("Authorization") String authorization,
            @PartMap Map map
    );



    @POST("dosti/v1/uploadImage")
    Call<Response> changeUserPhoto(@Header("Authorization") String token, @Body RequestBody photo);


    @Multipart
    @POST("dosti/v1/uploadImage")
    public Call<Void> upload(@Part("picture\"; filename=\"image\" ") RequestBody picture,
                             @Part("sort") RequestBody sort);

}