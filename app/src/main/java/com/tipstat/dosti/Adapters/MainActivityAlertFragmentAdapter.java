package com.tipstat.dosti.Adapters;

/**
 * Created by nikhi on 10/18/2016.
 */

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.tipstat.dosti.Activities.UserProfile_SomeActionActivity;
import com.tipstat.dosti.Models.MainActivityAlertFragmentModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.FaceDetector.FaceCrop;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivityAlertFragmentAdapter extends RecyclerView.Adapter<MainActivityAlertFragmentAdapter.MyViewHolder> {

    private List<MainActivityAlertFragmentModel> users;
    private FragmentActivity context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView status, name, age, date;
        public RelativeLayout relativeLayout;
        public ImageView profilepic;

        public MyViewHolder(View view) {
            super(view);

            status = (TextView) view.findViewById(R.id.status);
            name = (TextView) view.findViewById(R.id.name);
            age = (TextView) view.findViewById(R.id.age);
            date = (TextView) view.findViewById(R.id.date);
            profilepic = (ImageView) view.findViewById(R.id.profilepic);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relativelayout);
        }
    }


    public MainActivityAlertFragmentAdapter(List<MainActivityAlertFragmentModel> moviesList, FragmentActivity activity) {
        this.users = moviesList;
        this.context = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_mainactivityalertfragment, parent, false);

        return new MyViewHolder(itemView);
    }

    public String checkNukk(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else return "";
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final MainActivityAlertFragmentModel user = users.get(position);

        try {


            holder.name.setText(checkNukk(user.getName()));
            holder.status.setText(getStatus(checkNukk(user.getFlag()), checkNukk(user.getName())));
            if (user.getAge() != null && !user.getAge().isEmpty()) {


                if (Integer.parseInt(user.getAge()) > 0) {
                    holder.age.setText("Age " + checkNukk(user.getAge()) + ", " + checkNukk(user.getGender().substring(0, 1).toUpperCase() + user.getGender().substring(1)));

                } else {
                    holder.age.setText(checkNukk(user.getGender().substring(0, 1).toUpperCase() + user.getGender().substring(1)));

                }


            } else {
                holder.age.setText(checkNukk(user.getGender().substring(0, 1).toUpperCase() + user.getGender().substring(1)));
            }


            FaceCrop faceCrop = new FaceCrop(context);
            faceCrop.setFaceCropAsync(holder.profilepic, Uri.parse(Config.BaseImageUrl + user.getProfilepic_url()));
            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivityForResult(new Intent(context, UserProfile_SomeActionActivity.class).putExtra("userobject", user), 3);
                }
            });

            holder.profilepic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showProfilePic(Config.BaseImageUrl + user.getProfilepic_url());
                }
            });

            holder.date.setText(converDateTime(checkNukk(user.getDatetime())));
            //  Glide.with(context).load(user.getProfilepic_url()).into(holder.profilepic);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getStatus(String flag, String username) {
        if (flag.equalsIgnoreCase("2")) {
            return "Expressed interest in You";
        } else if (flag.equalsIgnoreCase("1")) {
            return "You have expressed interest";
        } else if (flag.equalsIgnoreCase("3"))
            return "Mutual Interest";
        else {
            String[] name = username.split("\\s+");
            if (name != null && name[0] != null && !name[0].isEmpty())
                return "You and " + checkNukk(name[0]) + " are friends now";
            else
                return "You both are friends";
        }
    }

    public String converDateTime(String dateTime) {
        try {


            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = f.parse(dateTime);
            String day = (String) android.text.format.DateFormat.format("dd", date);
            String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);
            String hours = (String) android.text.format.DateFormat.format("HH", date);
            String minutes = (String) android.text.format.DateFormat.format("mm", date);
            Log.d("time",date+"");
            return day + " " + stringMonth + ", " + hours + ":" + minutes;
        } catch (Exception e) {
            return "";
        }
    }

    public String convertTime(long time) {
        Log.d("long", time + "");
        Date date = new Date(time * 1000);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String day = (String) android.text.format.DateFormat.format("dd", date);
        String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);
        String hours = (String) android.text.format.DateFormat.format("HH", date);
        String minutes = (String) android.text.format.DateFormat.format("mm", date);

        return day + " " + stringMonth + ", " + hours + ":" + minutes;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }


    public void showProfilePic(String url) {
        Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(context);
        final ProgressBar progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleLarge);
        Glide.with(context).load(url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(imageView);

        progressBar.setVisibility(View.VISIBLE);

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.addContentView(progressBar, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.show();
    }
}