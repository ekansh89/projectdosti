package com.tipstat.dosti.Adapters;

/**
 * Created by nikhi on 10/18/2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import com.tipstat.dosti.Activities.UserProfile_SomeActionActivity;
import com.tipstat.dosti.Models.MainActivityFriendsListModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.FaceDetector.FaceCrop;

import java.util.List;

public class MainActivityFriendsListFragmentAdapter extends RecyclerView.Adapter<MainActivityFriendsListFragmentAdapter.MyViewHolder> {

    private List<MainActivityFriendsListModel> users;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, age, job, location;
        public ImageView profilepic;
        public RelativeLayout relativeLayout;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            age = (TextView) view.findViewById(R.id.age);
            job = (TextView) view.findViewById(R.id.profession);
            location = (TextView) view.findViewById(R.id.location);
            profilepic = (ImageView) view.findViewById(R.id.profilepic);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relativelayout);
        }
    }


    public MainActivityFriendsListFragmentAdapter(List<MainActivityFriendsListModel> moviesList, Context context) {
        this.users = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_mainactivityfriendslistfragment, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final MainActivityFriendsListModel user = users.get(position);
        holder.name.setText(checkNull(user.getName()));

        if(user!=null&& user.getAge()!=null)
        {
            if(Integer.parseInt(user.getAge())>0)
            {
                holder.age.setText("Age " + checkNull(user.getAge()) + ", " + checkNull(user.getGender().substring(0, 1).toUpperCase() + user.getGender().substring(1)));
            }
            else
            {
                holder.age.setText(checkNull(user.getGender().substring(0, 1).toUpperCase() + user.getGender().substring(1)));
            }
        }

        holder.job.setText(checkNull(user.getEducation()));
        holder.location.setVisibility(View.GONE);
        //  Glide.with(context).load(movie.getProfile_url()).into(holder.profilepic);

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, UserProfile_SomeActionActivity.class).putExtra("friendobject", user));
            }
        });
        FaceCrop faceCrop = new FaceCrop(context);
        faceCrop.setFaceCropAsync(holder.profilepic, Uri.parse(Config.BaseImageUrl+user.getProfile_url()));

        holder.profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProfilePic(Config.BaseImageUrl+user.getProfile_url());
            }
        });
    }


    public String checkNull(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "";
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    public void showProfilePic(String url) {
        Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(context);
        final ProgressBar progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleLarge);
        Glide.with(context).load(url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(imageView);

        progressBar.setVisibility(View.VISIBLE);

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.addContentView(progressBar, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.show();
    }

}