package com.tipstat.dosti.Adapters;

/**
 * Created by nikhi on 10/18/2016.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tipstat.dosti.R;

public class SplashScreenViewPagerAdapter extends PagerAdapter {

    private Context mContext;

    public SplashScreenViewPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_splashscreenviewpager, collection, false);
        TextView title = (TextView) layout.findViewById(R.id.title);
        TextView subtitle = (TextView) layout.findViewById(R.id.subtitle);
        ImageView imageView = (ImageView) layout.findViewById(R.id.image);
        if (position == 0) {
            title.setText("1. We'll pull some basic information from your Facebook profile");
            subtitle.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageView.setImageDrawable(mContext.getDrawable(R.drawable.viewpagerfirstimage));

            }
            else
            {
                imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.viewpagerfirstimage));
            }
        } else if (position == 1) {
            title.setText("2. We find you a like-minded friend from across the border");
            subtitle.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageView.setImageDrawable(mContext.getDrawable(R.drawable.viewpagerthirdimage));
            }
            else
            {
                imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.viewpagerthirdimage));
            }
        } else if (position == 2) {
            title.setText("3. Connect with your new friends using Facebook");
            subtitle.setVisibility(View.GONE);
            SpannableString spanString = new SpannableString("ACROSS THE BORDER");
            spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0, spanString.length(), 0);
            subtitle.setText(spanString);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageView.setImageDrawable(mContext.getDrawable(R.drawable.viewpagersecondimage));
            }
            else
            {
                imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.viewpagersecondimage));
            }
        }

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return position + "";
    }

}
