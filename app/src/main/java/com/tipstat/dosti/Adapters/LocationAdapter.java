package com.tipstat.dosti.Adapters;

/**
 * Created by nikhi on 10/20/2016.
 */

import java.util.List;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.tipstat.dosti.Models.LocationModel;
import com.tipstat.dosti.R;

public class LocationAdapter extends
        RecyclerView.Adapter<LocationAdapter.ViewHolder> {

    private List<LocationModel> stList;

    public LocationAdapter(List<LocationModel> students) {
        this.stList = students;

    }

    // Create new views
    @Override
    public LocationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_item_locationpreferences, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        final int pos = position;

        viewHolder.tvName.setText("");


        viewHolder.chkSelected.setChecked(stList.get(position).isSelected());

        viewHolder.chkSelected.setTag(stList.get(position));
        viewHolder.chkSelected.setText(stList.get(position).getCity() + ", " + stList.get(position).getState());

        viewHolder.chkSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                LocationModel contact = (LocationModel) cb.getTag();

                contact.setSelected(cb.isChecked());
                stList.get(pos).setSelected(cb.isChecked());


            }
        });

    }

    // Return the size arraylist
    @Override
    public int getItemCount() {
        return stList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;


        public CheckBox chkSelected;

        public LocationModel singlestudent;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tvName = (TextView) itemLayoutView.findViewById(R.id.tvName);


            chkSelected = (CheckBox) itemLayoutView
                    .findViewById(R.id.chkSelected);

        }

    }

    // method to access in activity after updating selection
    public List<LocationModel> getStudentist() {
        return stList;
    }

}
