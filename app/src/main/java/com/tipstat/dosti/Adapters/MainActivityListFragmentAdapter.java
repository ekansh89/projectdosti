package com.tipstat.dosti.Adapters;

/**
 * Created by nikhi on 10/18/2016.
 */

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tipstat.dosti.Activities.MainActivity;
import com.tipstat.dosti.Activities.UserProfileActivity;
import com.tipstat.dosti.Models.MainActivityListFragmentModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.FaceDetector.FaceCrop;

import java.util.List;


public class MainActivityListFragmentAdapter extends RecyclerView.Adapter<MainActivityListFragmentAdapter.MyViewHolder> {

    private MainActivity mContext;
    private List<MainActivityListFragmentModel> UsersList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView thumbnail;
        public RelativeLayout cardView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            count = (TextView) view.findViewById(R.id.count);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            cardView = (RelativeLayout) view.findViewById(R.id.relativelayout);

        }
    }


    public MainActivityListFragmentAdapter(MainActivity mContext, List<MainActivityListFragmentModel> albumList) {
        this.mContext = mContext;
        this.UsersList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_mainactivitylistfragment, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final MainActivityListFragmentModel user = UsersList.get(position);
        if(user!=null && user.getAge()!=null)
        {
            if(Integer.parseInt(user.getAge())>0)
            {
                holder.title.setText(user.getName()+ ", " + user.getAge());
            }
            else
            {
                holder.title.setText(user.getName());
            }
        }
        else
        {
            holder.title.setText(user.getName());
        }

        if(user.getCity()!=null && !user.getCity().isEmpty())
        {
            holder.count.setVisibility(View.VISIBLE);
            holder.count.setText("@ " + user.getCity() );
        }
        else
        {
            holder.count.setVisibility(View.INVISIBLE);
        }

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivityForResult(new Intent(mContext, UserProfileActivity.class).putExtra("listobject", user), 2);
            }
        });

        // loading album cover using Glide library
        // Glide.with(mContext).load(user.getProfile_url()).into(holder.thumbnail);
      /*   Glide.with(mContext)
                .load(user.getProfile_url())
                .transform(new FaceCrop())
                .into(holder.thumbnail);*/

        FaceCrop faceCrop = new FaceCrop(mContext);
        faceCrop.setFaceCropAsync(holder.thumbnail, Uri.parse(Config.BaseImageUrl+user.getProfile_url()));

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("MyAdapter", "onActivityResult");
    }

    @Override
    public int getItemCount() {
        return UsersList.size();
    }
}
