package com.tipstat.dosti.Models;

import com.plumillonforge.android.chipview.Chip;

/**
 * Created by Nikil on 12/22/2016.
 */
public class ChipTag implements Chip {
    private String mName;
    private int mType = 0;

    public ChipTag(String name, int type) {
        this(name);
        mType = type;
    }

    public ChipTag(String name) {
        mName = name;
    }

    @Override
    public String getText() {
        return mName;
    }

    public int getType() {
        return mType;
    }
}
