package com.tipstat.dosti.Models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nikhi on 10/18/2016.
 */

public class MainActivityAlertFragmentModel implements Serializable {

    private String age;
    private String datetime;
    private String fb_id2;
    private String flag;
    private String gender;
    private String interest;
    private String name;

    public List<MainActivityAlertFragmentModel> getUsers() {
        return users;
    }

    public void setUsers(List<MainActivityAlertFragmentModel> users) {
        this.users = users;
    }

    private List<MainActivityAlertFragmentModel> users;
    public String getProfilepic_url() {
        return profilepic_url;
    }

    public void setProfilepic_url(String profilepic_url) {
        this.profilepic_url = profilepic_url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String profilepic_url;
    private String status;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getFb_id2() {
        return fb_id2;
    }

    public void setFb_id2(String fb_id2) {
        this.fb_id2 = fb_id2;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}