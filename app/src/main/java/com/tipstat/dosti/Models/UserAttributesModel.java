package com.tipstat.dosti.Models;

import java.util.List;

/**
 * Created by a on 11/18/2016.
 */

public class UserAttributesModel {

    private List<String> language,r_status,religion;

    public List<String> getLanguage() {
        return language;
    }

    public void setLanguage(List<String> language) {
        this.language = language;
    }

    public List<String> getR_status() {
        return r_status;
    }

    public void setR_status(List<String> r_status) {
        this.r_status = r_status;
    }

    public List<String> getReligion() {
        return religion;
    }

    public void setReligion(List<String> religion) {
        this.religion = religion;
    }
}
