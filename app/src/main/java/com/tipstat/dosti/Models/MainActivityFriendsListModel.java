package com.tipstat.dosti.Models;


import java.io.Serializable;
import java.util.List;

/**
 * Created by nikhi on 10/18/2016.
 */

public class MainActivityFriendsListModel implements Serializable {

    private String age;
    private String education;
    private String fbId;
    private String gender;
    private String name;

    public String getProfile_url() {
        return profilepic_url;
    }

    public void setProfile_url(String profile_url) {
        this.profilepic_url = profile_url;
    }

    private String profilepic_url;

    public List<MainActivityFriendsListModel> getUsers() {
        return users;
    }

    public void setUsers(List<MainActivityFriendsListModel> users) {
        this.users = users;
    }

    private List<MainActivityFriendsListModel> users;
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
