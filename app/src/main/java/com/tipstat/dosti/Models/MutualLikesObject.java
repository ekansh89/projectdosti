package com.tipstat.dosti.Models;

/**
 * Created by a on 12/14/2016.
 */

public class MutualLikesObject {
    private String name,id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
