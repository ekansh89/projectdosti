package com.tipstat.dosti.Models;

import com.tipstat.dosti.RequestModels.SetPreferncesRequest;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nikhi on 10/21/2016.
 */

public class LoginModel implements Serializable {


    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    private SetPreferncesRequest preflocations;

    public SetPreferncesRequest getPreflocations() {
        return preflocations;
    }

    public void setPreflocations(SetPreferncesRequest preflocations) {
        this.preflocations = preflocations;
    }

    private String likes;

    public List<String> getMutualLikes() {
        return mutualLikes;
    }

    public void setMutualLikes(List<String> mutualLikes) {
        this.mutualLikes = mutualLikes;
    }

    private List<String> mutualLikes;

    public LoginModel getUser() {
        return user;
    }

    public void setUser(LoginModel user) {
        this.user = user;
    }

    private LoginModel user;


    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public String getRelationship_status() {
        return relationship_status;
    }

    public void setRelationship_status(String relationship_status) {
        this.relationship_status = relationship_status;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    private String age;
    private String city;
    private String country;
    private String education;
    private String fbId;
    private String gender;
    private String hometown;
    private String languages;
    private String latitude;
    private String longitude;
    private String about;
    private String pref_gender;

    public String getPref_city() {
        return pref_city;
    }

    public void setPref_city(String pref_city) {
        this.pref_city = pref_city;
    }

    private String pref_city;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    private String flag;

    public String getPref_min_age() {
        return pref_min_age;
    }

    public void setPref_min_age(String pref_min_age) {
        this.pref_min_age = pref_min_age;
    }

    public String getPref_gender() {
        return pref_gender;
    }

    public void setPref_gender(String pref_gender) {
        this.pref_gender = pref_gender;
    }

    public String getPref_max_age() {
        return pref_max_age;
    }

    public void setPref_max_age(String pref_max_age) {
        this.pref_max_age = pref_max_age;
    }

    private String pref_min_age;
    private String pref_max_age;

    public String getWork_role() {
        return work_role;
    }

    public void setWork_role(String work_role) {
        this.work_role = work_role;
    }

    public String getWork_company() {
        return work_company;
    }

    public void setWork_company(String work_company) {
        this.work_company = work_company;
    }

    private String work_role;
    private String work_company;

    public String getPref_country() {
        return pref_country;
    }

    public void setPref_country(String pref_country) {
        this.pref_country = pref_country;
    }

    private String pref_country;

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    private String birthday;

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    private String name;
    private String profile_url;
    private String relationship_status;
    private String religion;
    private String state;
    private String userID;
}
