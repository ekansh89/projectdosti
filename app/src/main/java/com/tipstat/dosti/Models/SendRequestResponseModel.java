package com.tipstat.dosti.Models;

/**
 * Created by a on 10/25/2016.
 */

public class SendRequestResponseModel {

    private String
            name,
            registration_id,
            status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegistration_id() {
        return registration_id;
    }

    public void setRegistration_id(String registration_id) {
        this.registration_id = registration_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
