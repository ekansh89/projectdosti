package com.tipstat.dosti.Fragments;

/**
 * Created by nikhi on 10/18/2016.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.ProfileTracker;
import com.tipstat.dosti.Activities.MainActivity;
import com.tipstat.dosti.R;
import com.tipstat.dosti.Utils.Config;

public class MainAcitivtyPrimaryFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 4;
    private String name;
    private MainActivityListFragment mainActivityListFragment;
    private MainActivityAlertsFragment mainActivityAlertsFragment;
    private MainActivityUserProfileFragment mainActivityUserProfileFragment;
    private MainActivityFriendsListFragment mainActivityFriendsListFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View x = inflater.inflate(R.layout.layout_mainactivitytab, null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        if (getArguments() != null)
            name = getArguments().getString("name");
        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        viewPager.setOffscreenPageLimit(3);
        //viewPager.setOffscreenPageLimit(4);
        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
                for (int i = 0; i < tabLayout.getTabCount(); i++) {

                    if (i == 0) {
                        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.mipmap.homeselected));
                    } else if (i == 1) {
                        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.mipmap.alertunselected));
                    } else if (i == 2) {
                        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.mipmap.personunselected));
                    } else if (i == 3) {
                        tabLayout.getTabAt(3).setIcon(getResources().getDrawable(R.mipmap.friendsunselected));
                    }

                }
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                try {
                    if (position == 0) {
                        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.mipmap.homeselected));
                        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.mipmap.alertunselected));
                        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.mipmap.personunselected));
                        tabLayout.getTabAt(3).setIcon(getResources().getDrawable(R.mipmap.friendsunselected));
                        ((MainActivity) getActivity()).returnTitleView().setText("PROJECT DOSTI");
                    } else if (position == 1) {
                        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.mipmap.homeunselected));
                        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.mipmap.alertselected));
                        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.mipmap.personunselected));
                        tabLayout.getTabAt(3).setIcon(getResources().getDrawable(R.mipmap.friendsunselected));
                        ((MainActivity) getActivity()).returnTitleView().setText("PROJECT DOSTI");
                    } else if (position == 2) {
                        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.mipmap.homeunselected));
                        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.mipmap.alertunselected));
                        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.mipmap.personselected));
                        tabLayout.getTabAt(3).setIcon(getResources().getDrawable(R.mipmap.friendsunselected));
                        mainActivityUserProfileFragment.scrollUp();
                        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
                        String usernmae = pref.getString("username", "");

                        if (usernmae != null && !usernmae.isEmpty()) {
                            ((MainActivity) getActivity()).returnTitleView().setVisibility(View.VISIBLE);
                            ((MainActivity) getActivity()).returnTitleView().setText(usernmae + "");
                        } else if (name != null && !name.isEmpty()) {
                            ((MainActivity) getActivity()).returnTitleView().setVisibility(View.VISIBLE);
                            ((MainActivity) getActivity()).returnTitleView().setText(name + "");
                        } else {
                            ((MainActivity) getActivity()).returnTitleView().setVisibility(View.GONE);
                        }

                    } else if (position == 3) {
                        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.mipmap.homeunselected));
                        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.mipmap.alertunselected));
                        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.mipmap.personunselected));
                        tabLayout.getTabAt(3).setIcon(getResources().getDrawable(R.mipmap.friendsselected));
                        ((MainActivity) getActivity()).returnTitleView().setText("PROJECT DOSTI");
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return x;

    }

    public void moveViewPager() {
        if(viewPager!=null) {
            viewPager.setCurrentItem(1);
            mainActivityAlertsFragment.getData();
            mainActivityFriendsListFragment.getData();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("from parent", "True");
        mainActivityListFragment.onActivityResult(requestCode, resultCode, data);
        mainActivityFriendsListFragment.onActivityResult(requestCode, resultCode, data);
        mainActivityAlertsFragment.onActivityResult(requestCode, resultCode, data);
        mainActivityUserProfileFragment.onActivityResult(requestCode, resultCode, data);
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    mainActivityListFragment = new MainActivityListFragment();
                    return mainActivityListFragment;
                case 1:
                    mainActivityAlertsFragment = new MainActivityAlertsFragment();
                    return mainActivityAlertsFragment;
                case 2:
                    mainActivityUserProfileFragment = new MainActivityUserProfileFragment();
                    return mainActivityUserProfileFragment;
                case 3:
                    mainActivityFriendsListFragment = new MainActivityFriendsListFragment();
                    return mainActivityFriendsListFragment;
            }
            return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "";
                case 1:
                    return "";
                case 2:
                    return "";
                case 3:
                    return "";
            }
            return null;
        }
    }

}