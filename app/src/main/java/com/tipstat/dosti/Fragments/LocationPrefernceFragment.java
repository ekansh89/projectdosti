package com.tipstat.dosti.Fragments;

/**
 * Created by nikhi on 10/20/2016.
 */

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.google.gson.Gson;
import com.tipstat.dosti.Activities.SetPreferencesActivity;
import com.tipstat.dosti.Adapters.LocationAdapter;
import com.tipstat.dosti.Models.LocationModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.SetPreferncesRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.ChipView.ChipsView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * Created by MG on 17-07-2016.
 */
public class LocationPrefernceFragment extends BottomSheetDialogFragment  {

    String mString;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Button saveLocation;
    private List<LocationModel> locationModels;
    private static BottomSheetBehavior mBottomSheetBehavior;
    private ProgressBar progressBar;
    private EditText locationSearch;
    private static List<String> chipslist;

    public static LocationPrefernceFragment newInstance(String string, List<String> chips) {
        LocationPrefernceFragment f = new LocationPrefernceFragment();
        Bundle args = new Bundle();
        args.putString("string", string);
        chipslist = chips;
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mString = getArguments().getString("string");
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View v = View.inflate(getContext(), R.layout.layout_fragment_locationpreferences, null);
        dialog.setContentView(v);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) v.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            mBottomSheetBehavior = (BottomSheetBehavior) behavior;
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {


                    if (newState == 5)
                        getDialog().cancel();

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    v.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    int height = v.getMeasuredHeight();
                    mBottomSheetBehavior.setPeekHeight(height);
                }
            });
        }


        mRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        saveLocation = (Button) v.findViewById(R.id.savelocation);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        locationSearch = (EditText) v.findViewById(R.id.searchlocation);

        saveLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String data = "";
                getDialog().cancel();
                List<LocationModel> stList = ((LocationAdapter) mAdapter)
                        .getStudentist();

              /*  List<LocationModel> temp = new ArrayList<LocationModel>();
                for (int i = 0; i < stList.size(); i++) {
                    if (stList.get(i).isSelected()) {
                        temp.add(stList.get(i));
                    }
                }*/


                ((SetPreferencesActivity) getActivity()).setLocations(stList);
            }
        });
        getLocations();


        locationSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 0) {

                    mRecyclerView.setHasFixedSize(true);

                    // use a linear layout manager
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                    // create an Object for Adapter
                    mAdapter = new LocationAdapter(locationModels);

                    // set the adapter object to the Recyclerview
                    mRecyclerView.setAdapter(mAdapter);

                } else {
                    List<LocationModel> temp = new ArrayList<LocationModel>();
                    for (int j = 0; j < locationModels.size(); j++) {
                        if (locationModels.get(j).getCity().matches("(?i:.*" + locationSearch.getText().toString() + ".*)") || locationModels.get(j).getState().matches("(?i:.*" + locationSearch.getText().toString() + ".*)")) {
                            temp.add(locationModels.get(j));
                        }

                    }


                    mRecyclerView.setHasFixedSize(true);

                    // use a linear layout manager
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                    // create an Object for Adapter
                    mAdapter = new LocationAdapter(temp);

                    // set the adapter object to the Recyclerview
                    mRecyclerView.setAdapter(mAdapter);

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }




    public void getLocations() {

        progressBar.setVisibility(View.VISIBLE);
        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        String userpreferences = pref.getString("userpreferences", null);
        String userCountry = pref.getString("usercountry", null);

        Map<String, String> params = new HashMap<String, String>();
        if (userCountry != null && !userCountry.isEmpty() && (userCountry.equalsIgnoreCase("india") || userCountry.equalsIgnoreCase("pakistan") || userCountry.equalsIgnoreCase("pakisthan"))) {
            if (userCountry.equalsIgnoreCase("india"))
                params.put("country", "pakistan");
            else
                params.put("country", "india");
        } else {
            Gson gson = new Gson();
            SetPreferncesRequest setPreferncesRequests = gson.fromJson(userpreferences, SetPreferncesRequest.class);
            if (setPreferncesRequests != null && setPreferncesRequests.getCountry() != null && !setPreferncesRequests.getCountry().isEmpty()) {
                if (setPreferncesRequests.getCountry().equalsIgnoreCase("pakistan"))
                    params.put("country", "pakistan");
                else params.put("country", "india");
            } else {
                params.put("country", "");
            }
        }

        Call cm = new RequestApi().getInterface().getLocationPreference(params);

        cm.enqueue(new Callback<LocationModel>() {
            @Override
            public void onResponse(Response<LocationModel> response) {
                progressBar.setVisibility(View.GONE);
                if(response != null && response.body() != null ) {
                    locationModels = response.body().getLocations();

                    if (locationModels != null && locationModels.size() > 0) {

                        locationSearch.setVisibility(View.VISIBLE);

                        mRecyclerView.setVisibility(View.VISIBLE);
                        saveLocation.setVisibility(View.VISIBLE);
                        // use this setting to improve performance if you know that changes
                        // in content do not change the layout size of the RecyclerView
                        mRecyclerView.setHasFixedSize(true);

                        // use a linear layout manager
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                        for (int i = 0; i < locationModels.size(); i++) {
                            if (chipslist != null && chipslist.contains(locationModels.get(i).getCity() + ", " + locationModels.get(i).getState())) {
                                locationModels.get(i).setSelected(true);
                            }
                        }
                        // create an Object for Adapter
                        mAdapter = new LocationAdapter(locationModels);

                        // set the adapter object to the Recyclerview
                        mRecyclerView.setAdapter(mAdapter);
                    } else {
                        locationSearch.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.GONE);
                        saveLocation.setVisibility(View.GONE);

                    }
                }
                else
                {
                    Toast.makeText(getActivity(),"Something went wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                mRecyclerView.setVisibility(View.GONE);
                locationSearch.setVisibility(View.GONE);
                saveLocation.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }


        });
    }
}