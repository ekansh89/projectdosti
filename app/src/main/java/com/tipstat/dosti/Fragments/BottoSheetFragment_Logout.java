package com.tipstat.dosti.Fragments;

/**
 * Created by nikhi on 10/20/2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.Button;

import com.facebook.login.LoginManager;
import com.tipstat.dosti.Activities.SplashActivity;
import com.tipstat.dosti.R;
import com.tipstat.dosti.Utils.Config;

/**
 * Created by MG on 17-07-2016.
 */
public class BottoSheetFragment_Logout extends BottomSheetDialogFragment {

    String mString;
    private static BottomSheetBehavior mBottomSheetBehavior;

    public static BottoSheetFragment_Logout newInstance(String string) {
        BottoSheetFragment_Logout f = new BottoSheetFragment_Logout();
        Bundle args = new Bundle();
        args.putString("string", string);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mString = getArguments().getString("string");
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View v = View.inflate(getContext(), R.layout.layout_fragment_bottomsheet_logout, null);
        dialog.setContentView(v);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) v.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            mBottomSheetBehavior = (BottomSheetBehavior) behavior;
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            Button yes = (Button) v.findViewById(R.id.yes);
            Button no = (Button) v.findViewById(R.id.no);

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoginManager.getInstance().logOut();
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.SHARED_PREF, Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("username", "");
                    editor.putString("usercountry",  "");
                    editor.putString("userpreferences", "");
                    editor.putString("userpreferedcountry","");
                    editor.commit();
                    startActivity(new Intent(getActivity(), SplashActivity.class));
                    getActivity().finish();
                }
            });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().cancel();
                }
            });
        }


    }

   /* @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_fragment_locationpreferences, container, false);




        return v;
    }*/
}