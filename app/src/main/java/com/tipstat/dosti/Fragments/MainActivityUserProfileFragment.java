package com.tipstat.dosti.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.facebook.AccessToken;

import com.google.gson.Gson;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Picasso;
import com.tipstat.dosti.Activities.SetPreferencesActivity;
import com.tipstat.dosti.Activities.UserProfileEdit;
import com.tipstat.dosti.Activities.UserProfile_SomeActionActivity;
import com.tipstat.dosti.Models.ChipTag;
import com.tipstat.dosti.Models.LoginModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.FcmIdRequest;
import com.tipstat.dosti.RequestModels.LoginApiRequest;
import com.tipstat.dosti.RequestModels.SetPreferncesRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.ChipView.ChipsView;
import com.tipstat.dosti.Views.ChipView.Contact;
import com.tipstat.dosti.Views.ErrorView.ErrorView;
import com.tipstat.dosti.Views.FaceDetector.FaceCrop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


/**
 * Created by nikhi on 10/19/2016.
 */

public class MainActivityUserProfileFragment extends Fragment {


    private com.tipstat.dosti.Views.ChipView.ChipsView mChipsView;
    private FloatingActionButton editprofile;
    private ImageView profilepicture;
    private TextView location, job, company, gender, languages, religion, relationship, hometown, aboutme, languagetitle;
    private ProgressBar progressBar;
    private ScrollView scrollView;
    private View view, view1;
    private RelativeLayout religionlayout, relationshiplayout, hometownlayout;
    private TextView mutuallikestitle;
    private ErrorView errorView;
    private List<String> likes;
    private ChipView chip;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_mainactivityuserprofilefragment, null);

        mChipsView = (ChipsView) view.findViewById(R.id.cv_contacts);
        editprofile = (FloatingActionButton) view.findViewById(R.id.editprofile);
        profilepicture = (ImageView) view.findViewById(R.id.thumbnail);
        location = (TextView) view.findViewById(R.id.location);
        job = (TextView) view.findViewById(R.id.designation);
        company = (TextView) view.findViewById(R.id.organisation);
        gender = (TextView) view.findViewById(R.id.gender);
        aboutme = (TextView) view.findViewById(R.id.aboutme);
        hometown = (TextView) view.findViewById(R.id.hometown);
        relationship = (TextView) view.findViewById(R.id.relationship);
        languagetitle = (TextView) view.findViewById(R.id.languagetitle);
        this.view = view.findViewById(R.id.view);
        this.view1 = view.findViewById(R.id.view1);
        religion = (TextView) view.findViewById(R.id.religion);
        languages = (TextView) view.findViewById(R.id.language);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        relationshiplayout = (RelativeLayout) view.findViewById(R.id.relationshiplayout);
        religionlayout = (RelativeLayout) view.findViewById(R.id.religionlayout);
        hometownlayout = (RelativeLayout) view.findViewById(R.id.hometownlayout);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        mutuallikestitle = (TextView) view.findViewById(R.id.mutualikestitle);
        errorView = (ErrorView) view.findViewById(R.id.errorview);
        chip = (ChipView) view.findViewById(R.id.text_chip_attrs);
        // change EditText config
        mChipsView.getEditText().setCursorVisible(false);


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {

                errorView.setVisibility(View.GONE);
                getData();
            }
        });
        getData();
        return view;

    }


    public void getData() {

        if (isAdded()) {
            progressBar.setVisibility(View.VISIBLE);


            SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
            LoginApiRequest loginApiRequest = new LoginApiRequest();
            loginApiRequest.setAccess_token(AccessToken.getCurrentAccessToken().getToken() + "");
            loginApiRequest.setFb_id(AccessToken.getCurrentAccessToken().getUserId() + "");
            loginApiRequest.setFcm_id(pref.getString("regId", null));
            // loginApiRequest.setCountry(countryISOCode);

            final Call<LoginModel> cm = new RequestApi().getInterface().login(loginApiRequest);

            cm.enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(Response<LoginModel> response) {

                    try {
                        if (isAdded()) {
                            progressBar.setVisibility(View.GONE);
                            scrollView.setVisibility(View.VISIBLE);
                            editprofile.setVisibility(View.VISIBLE);
                            errorView.setVisibility(View.GONE);
                            final LoginModel user = response.body().getUser();

                            if (user.getLikes() != null && !user.getLikes().isEmpty())
                                likes = Arrays.asList(user.getLikes().split(","));

                            SetPreferncesRequest setPreferncesRequest = new SetPreferncesRequest();

                            if (user.getPref_country() != null && !user.getPref_country().isEmpty()) {
                              setPreferncesRequest.setCountry(user.getPref_country());
                            }
                            if (user.getPref_gender() != null && !user.getPref_gender().isEmpty()) {
                                setPreferncesRequest.setGender(user.getPref_gender());
                            }
                            if (user.getPref_max_age() != null && !user.getPref_max_age().isEmpty()) {
                                setPreferncesRequest.setMaxage(user.getPref_max_age());
                            }
                            if (user.getPref_min_age() != null && !user.getPref_min_age().isEmpty()) {
                                setPreferncesRequest.setMinage(user.getPref_min_age());
                            }
                            if(user.getPref_city()!=null && !user.getPref_city().isEmpty())
                            {
                                List<String> cities = Arrays.asList(user.getPref_city().split(","));
                                setPreferncesRequest.setLocations(cities);
                            }

                            if (user != null && user.getCountry() != null && !user.getCountry().isEmpty() && (user.getCountry().equalsIgnoreCase("india") || user.getCountry().equalsIgnoreCase("pakistan") || user.getCountry().equalsIgnoreCase("pakisthan"))) {
                                printUserDetails(user, likes);
                                Gson gson = new Gson();
                                SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("username", user.getName());
                                editor.putString("usercountry", user.getCountry() + "");
                                editor.putString("userpreferences", gson.toJson(setPreferncesRequest));
                                editor.commit();
                                Log.d("preferences", gson.toJson(setPreferncesRequest)+"\t"+setPreferncesRequest.getCountry());
                            } else {

                                if (user.getPref_country() != null && !user.getPref_country().isEmpty()) {
                                    printUserDetails(user, likes);
                                    Gson gson = new Gson();
                                    SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString("username", user.getName());
                                    editor.putString("usercountry", user.getCountry() + "");
                                    editor.putString("userpreferences", gson.toJson(setPreferncesRequest));
                                    editor.commit();
                                    Log.d("preferences", gson.toJson(setPreferncesRequest));
                                } else {
                                    startActivity(new Intent(getActivity(), SetPreferencesActivity.class).putExtra("nocoutry", true));
                                    getActivity().finish();
                                }
                            }

                        } else {
                            Log.d("fragment", "no activty");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressBar.setVisibility(View.GONE);
                        scrollView.setVisibility(View.GONE);
                        editprofile.setVisibility(View.GONE);
                        errorView.setVisibility(View.VISIBLE);
                        errorView.setSubtitle("Unable to connect server");
                        errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                        errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));

                    }

                }

                @Override
                public void onFailure(Throwable t) {
                    if (isAdded()) {
                        progressBar.setVisibility(View.GONE);
                        scrollView.setVisibility(View.GONE);
                        editprofile.setVisibility(View.GONE);
                        errorView.setVisibility(View.VISIBLE);
                        errorView.setSubtitle("Unable to connect server");
                        errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                        errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));

                        Log.d("failure", t.getLocalizedMessage() + "");
                    } else {
                        Log.d("fragment", "no activty");
                    }
                }


            });

        }
    }


    public void printUserDetails(final LoginModel user, List<String> likes) {

        try {


            if (user != null) {


                editprofile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), UserProfileEdit.class);
                        intent.putExtra("userobject", user);
                        startActivityForResult(intent, 501);
                    }
                });
                if (user.getCity() != null && !user.getCity().isEmpty()) {
                    location.setVisibility(View.VISIBLE);
                    location.setText("  " + checkNull(user.getCity()));
                    if (user.getCountry() != null && !user.getCountry().isEmpty()) {
                        location.setText(location.getText() + ", " + checkNull(user.getCountry().substring(0, 1).toUpperCase() + user.getCountry().substring(1) + ""));
                    }

                } else
                    location.setVisibility(View.GONE);

                if (user.getGender() != null && !user.getGender().isEmpty()) {
                    gender.setVisibility(View.VISIBLE);
                    gender.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                    gender.setText("  " + checkNull(user.getGender().substring(0, 1).toUpperCase() + user.getGender().substring(1) + ""));
                } else {
                    gender.setVisibility(View.GONE);
                }

                if (user.getAbout() != null && !user.getAbout().isEmpty()) {
                    aboutme.setVisibility(View.VISIBLE);
                    aboutme.setText(checkNull(user.getAbout() + ""));
                } else {
                    aboutme.setVisibility(View.GONE);
                }

                if (user.getLanguages() != null && !user.getLanguages().isEmpty()) {
                    languages.setVisibility(View.VISIBLE);
                    languagetitle.setVisibility(View.VISIBLE);
                    view.setVisibility(View.VISIBLE);
                    view1.setVisibility(View.VISIBLE);
                    languages.setText(checkNull(user.getLanguages().replace(",",", ") + ""));
                } else {
                    languages.setVisibility(View.GONE);
                    languagetitle.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    view1.setVisibility(View.GONE);
                }


                relationship.setText(checkNull(user.getRelationship_status()));
                hometown.setText(checkNull(user.getHometown() + ""));
                religion.setText(checkNull(user.getReligion() + ""));

                AdjustLayout(user);


                if (user.getProfile_url() != null && !user.getProfile_url().isEmpty()) {
                                   /* Picasso.with(getContext())
                                            .load(user.getProfile_url())
                                            .placeholder(getResources().getDrawable(R.mipmap.ic_person))
                                            .into(profilepicture, new com.squareup.picasso.Callback() {
                                                @Override
                                                public void onSuccess() {
                                                    // mPersonIcon.setVisibility(View.INVISIBLE);
                                                }

                                                @Override
                                                public void onError() {

                                                }
                                            });*/

                    FaceCrop faceCrop = new FaceCrop(getActivity());
                    faceCrop.setFaceCropAsync(profilepicture, Uri.parse(Config.BaseImageUrl + user.getProfile_url()));

                    profilepicture.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showProfilePic(Config.BaseImageUrl + user.getProfile_url());
                        }
                    });
                } else {
                    profilepicture.setImageDrawable(getResources().getDrawable(R.drawable.ic_person_24dp));
                }

                int likesSize = 0;

                if (likes != null && likes.size() > 0) {


                    //limiting likes to show only 100 page names or less than 100 page names
                    if (likes.size() < 100) {
                        likesSize = likes.size();
                    } else {
                        likesSize = 100;
                    }


                    mutuallikestitle.setVisibility(View.VISIBLE);
                    // mChipsView.setVisibility(View.VISIBLE);
                                   /* if (likes.size() < 10)
                                        likesSize = likes.size();
                                    else
                                        likesSize = 10;
*/
                    List<Chip> chipList = new ArrayList<>();

                    chip.setChipBackgroundColor(getResources().getColor(R.color.base10));
                    chip.setChipBackgroundColorSelected(getResources().getColor(R.color.base10));
                    chip.setChipPadding(20);
                    // chip.setChipBackgroundRes(R.drawable.black_border_white);


                    for (int i = 0; i < likesSize; i++) {
                        final String email = likes.get(i);
                       /* final Uri imgUrl = Uri.parse("https://robohash.org/" + Math.abs(email.hashCode()));
                        final Contact contact = new Contact(null, null, null, email, imgUrl);

                        mChipsView.addChip(email, imgUrl, contact);*/

                        chipList.add(new ChipTag(email));


                    }

                    chip.setChipList(chipList);


                } else {
                    mutuallikestitle.setVisibility(View.GONE);
                    mChipsView.setVisibility(View.GONE);
                }


                //  String[] userJobDetails = user.getEducation().split("@");

                if (user.getWork_company() != null && !user.getWork_company().isEmpty()) {
                    job.setVisibility(View.VISIBLE);
                    company.setVisibility(View.VISIBLE);
                    job.setText(checkNull(user.getWork_role()) + "");
                    company.setText("@ " + checkNull(user.getWork_company()) + "");
                } else {
                    if (user.getWork_role() != null && !user.getWork_role().isEmpty()) {
                        job.setVisibility(View.VISIBLE);
                        company.setVisibility(View.INVISIBLE);
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) job.getLayoutParams();
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                        job.setLayoutParams(params);
                        job.setText(checkNull(user.getWork_role()) + "");
                    } else {
                        job.setVisibility(View.INVISIBLE);
                        company.setVisibility(View.INVISIBLE);
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) location.getLayoutParams();
                        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                        location.setLayoutParams(params);
                    }
                }
                   /* if (userJobDetails != null) {
                        if (userJobDetails.length == 1) {
                            job.setVisibility(View.VISIBLE);
                            company.setVisibility(View.INVISIBLE);
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) job.getLayoutParams();
                            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                            job.setLayoutParams(params);
                            job.setText(checkNull(userJobDetails[0]) + "");
                        } else if (userJobDetails.length == 2) {
                            job.setVisibility(View.VISIBLE);
                            company.setVisibility(View.VISIBLE);
                            job.setText(checkNull(userJobDetails[0]) + "");
                            company.setText("@ " + checkNull(userJobDetails[1]) + "");
                        }


                } else {
                    job.setVisibility(View.INVISIBLE);
                    company.setVisibility(View.INVISIBLE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) location.getLayoutParams();
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    location.setLayoutParams(params);
                }
*/

            } else {
                scrollView.setVisibility(View.GONE);
                editprofile.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                errorView.setSubtitle("Unable to connect server");
                errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));

            }
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            editprofile.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
            errorView.setSubtitle("Unable to connect server");
            errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
            errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 501) {
          /*  if (data != null) {
                LoginModel user = (LoginModel) data.getSerializableExtra("userobject");
                if (user != null) {
                    Log.d("good data", "true");
                    printUserDetails(user, likes);
                } else
                    Log.d("null user", "true");
            } else
                Log.d("null data", "true");*/

            getData();
        } else {
            Log.d("null result", "true");
        }
    }

    public void showProfilePic(String url) {
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        final ImageView imageView = new ImageView(getActivity());
        final ProgressBar progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        Glide.with(getActivity()).load(url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(imageView);

        progressBar.setVisibility(View.VISIBLE);

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.addContentView(progressBar, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.show();
    }

    public void AdjustLayout(LoginModel user) {
        if (checkNull(user.getRelationship_status()).equalsIgnoreCase("na") && checkNull(user.getReligion()).equalsIgnoreCase("na") && user.getHometown().equalsIgnoreCase("na")) {
            relationshiplayout.setVisibility(View.GONE);
            religionlayout.setVisibility(View.GONE);
            hometownlayout.setVisibility(View.GONE);
        }

        if (checkNull(user.getRelationship_status()).equalsIgnoreCase("na")) {
            relationshiplayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.5f;
            religionlayout.setLayoutParams(params);
            relationshiplayout.setLayoutParams(params);

        }
        if (checkNull(user.getReligion()).equalsIgnoreCase("na")) {
            religionlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.5f;
            hometownlayout.setLayoutParams(params);
            relationshiplayout.setLayoutParams(params);

        }
        if (checkNull(user.getHometown()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.5f;
            religionlayout.setLayoutParams(params);
            relationshiplayout.setLayoutParams(params);
        }

        if (checkNull(user.getHometown()).equalsIgnoreCase("na") && checkNull(user.getReligion()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 3.0f;
            relationshiplayout.setLayoutParams(params);
        }

        if (checkNull(user.getHometown()).equalsIgnoreCase("na") && checkNull(user.getRelationship_status()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 3.0f;
            religionlayout.setLayoutParams(params);
        }

        if (checkNull(user.getReligion()).equalsIgnoreCase("na") && checkNull(user.getRelationship_status()).equalsIgnoreCase("na")) {
            hometownlayout.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 3.0f;
            hometownlayout.setLayoutParams(params);
        }

    }

    public void scrollUp() {
        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);


        if (!TextUtils.isEmpty(regId)) {
            Log.d("Firebase id", "Firebase reg id: " + regId);
            sendRegistrationToServer(regId);
        } else
            Log.d("Firebase id", "Firebase reg id not received yet " + regId);


    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e("Firebasenewtoiken", "sendRegistrationToServer: " + token);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //TODO your background code


                if (AccessToken.getCurrentAccessToken() != null) {

                    if (AccessToken.getCurrentAccessToken().getUserId() != null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {

                       /* TelephonyManager teleMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        String countryISOCode = "";

                        if (teleMgr != null) {
                            countryISOCode = teleMgr.getNetworkCountryIso();
                            if (teleMgr.getNetworkCountryIso().equalsIgnoreCase("in"))
                                countryISOCode = "india";
                            else
                                countryISOCode = "pakistan";
                        }*/


                        FcmIdRequest fcmIdRequest = new FcmIdRequest();
                        fcmIdRequest.setFb_id(AccessToken.getCurrentAccessToken().getUserId() + "");
                        // fcmIdRequest.setCountry(countryISOCode);
                        fcmIdRequest.setFcm(token);

                        Call cm = new RequestApi().getInterface().fcmregistartion(fcmIdRequest);

                        cm.enqueue(new Callback<FcmIdRequest>() {
                            @Override
                            public void onResponse(Response<FcmIdRequest> response) {

                            }

                            @Override
                            public void onFailure(Throwable t) {

                            }


                        });
                    } else
                        Log.d("fcmfromservice", "null string");
                } else
                    Log.d("fcmfromservice", "null access token");

            }
        });

    }

    public String checkNull(String string) {

        if (string != null && !string.isEmpty()) {
            return string;
        } else
            return "NA";
    }
}
