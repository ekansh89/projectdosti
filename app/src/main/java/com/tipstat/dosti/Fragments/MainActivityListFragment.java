package com.tipstat.dosti.Fragments;

/**
 * Created by nikhi on 10/18/2016.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.google.gson.Gson;
import com.tipstat.dosti.Activities.MainActivity;
import com.tipstat.dosti.Adapters.MainActivityListFragmentAdapter;
import com.tipstat.dosti.Models.MainActivityListFragmentModel;
import com.tipstat.dosti.Models.UsersListModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.FcmIdRequest;
import com.tipstat.dosti.RequestModels.SetPreferncesRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.ErrorView.ErrorView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


public class MainActivityListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private RecyclerView recyclerView;
    private MainActivityListFragmentAdapter adapter;
    private List<MainActivityListFragmentModel> list;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView title;
    private ErrorView errorView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_fragment_mainactivitylistfragment, null);


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        list = new ArrayList<>();
        adapter = new MainActivityListFragmentAdapter((MainActivity) getActivity(), list);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        errorView = (ErrorView) view.findViewById(R.id.errorview);
        title = (TextView) view.findViewById(R.id.title);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                if (recyclerView != null)
                    recyclerView.setVisibility(View.VISIBLE);
                title.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(true);
                getData();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        getData();
                                    }
                                }
        );


        // getData();

        return view;
    }

    /**
     * @param requestCode
     * @param resultCode
     * @param data
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 2 || resultCode == 3) {
            String id = data.getStringExtra("id");
            String flag = data.getStringExtra("flag");
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getFbId().equals(id)) {
                    list.get(i).setFlag(flag);
                }
            }
            adapter.notifyDataSetChanged();

        } else if (resultCode == 526) {
            Log.d("act result list frag", "True");
            swipeRefreshLayout.setRefreshing(true);
            list.clear();
            adapter.notifyDataSetChanged();
            getData();
        }
    }

    /**
     * function used to fetch people list basing upon the user prefernces
     */

    public void getData() {

        try {

            if (isAdded()) {
                SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
                String userpreferences = pref.getString("userpreferences", null);
                String userCountry = pref.getString("usercountry", null);

                Map<String, String> params = new HashMap<String, String>();
                params.put("fb_id", AccessToken.getCurrentAccessToken().getUserId() + "");
                params.put("access_token",AccessToken.getCurrentAccessToken().getToken());
                if (userCountry != null && !userCountry.isEmpty() && (userCountry.equalsIgnoreCase("india") || userCountry.equalsIgnoreCase("pakistan") || userCountry.equalsIgnoreCase("pakisthan"))) {
                    params.put("country", userCountry);
                } else {
                    Gson gson = new Gson();
                    SetPreferncesRequest setPreferncesRequests = gson.fromJson(userpreferences, SetPreferncesRequest.class);
                    if (setPreferncesRequests != null && setPreferncesRequests.getCountry() != null && !setPreferncesRequests.getCountry().isEmpty()) {
                        if (setPreferncesRequests.getCountry().equalsIgnoreCase("india"))
                            params.put("country", "pakistan");
                        else params.put("country", "india");
                    } else {
                        params.put("country", "");
                    }
                }
                Call<MainActivityListFragmentModel> cm = new RequestApi().getInterface().getPreferedUsersList(params);

                cm.enqueue(new Callback<MainActivityListFragmentModel>() {
                    @Override
                    public void onResponse(Response<MainActivityListFragmentModel> response) {
                        if (isAdded()) {
                            if (response != null && response.body() != null && response.body().getUserlist() != null && response.body().getUserlist().size() > 0) {
                                list.addAll(response.body().getUserlist());
                                Log.d("listsize", list.size() + "");
                                adapter.notifyDataSetChanged();
                                swipeRefreshLayout.setRefreshing(false);
                                recyclerView.setVisibility(View.VISIBLE);
                                title.setVisibility(View.VISIBLE);
                                errorView.setVisibility(View.GONE);
                            } else {
                                swipeRefreshLayout.setRefreshing(false);
                                recyclerView.setVisibility(View.GONE);
                                title.setVisibility(View.GONE);
                                errorView.setVisibility(View.VISIBLE);
                                errorView.setSubtitle("No friends Available");
                                errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                                errorView.setImage(getResources().getDrawable(R.mipmap.ic_sad));

                            }
                        }
                        else
                        {
                            Log.d("fragment","no activty");
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        if (isAdded()) {
                            swipeRefreshLayout.setRefreshing(false);
                            recyclerView.setVisibility(View.GONE);
                            title.setVisibility(View.GONE);
                            errorView.setVisibility(View.VISIBLE);
                            errorView.setSubtitle("Unable to connect server");
                            errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                            errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));
                        }
                        else
                        {
                            Log.d("fragment","no activty");
                        }
                    }


                });
            }

            else
            {
                Log.d("fragment","no activty");
            }

        } catch (Exception e) {

        }
    }


    @Override
    public void onRefresh() {
        list.clear();
        adapter.notifyDataSetChanged();
        getData();
    }


    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}