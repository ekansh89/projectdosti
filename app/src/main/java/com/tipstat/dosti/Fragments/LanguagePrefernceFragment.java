package com.tipstat.dosti.Fragments;

/**
 * Created by nikhi on 10/20/2016.
 */

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.tipstat.dosti.Activities.SetPreferencesActivity;
import com.tipstat.dosti.Adapters.LocationAdapter;
import com.tipstat.dosti.Models.LocationModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Views.CustomProgressDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * Created by MG on 17-07-2016.
 */
public class LanguagePrefernceFragment extends BottomSheetDialogFragment implements SwipeRefreshLayout.OnRefreshListener {

    String mString;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Button saveLocation;
    private List<LocationModel> locationModels;
    private static BottomSheetBehavior mBottomSheetBehavior;
    private SwipeRefreshLayout swipeRefreshLayout;
    private EditText locationSearch;

    public static LanguagePrefernceFragment newInstance(String string) {
        LanguagePrefernceFragment f = new LanguagePrefernceFragment();
        Bundle args = new Bundle();
        args.putString("string", string);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mString = getArguments().getString("string");
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View v = View.inflate(getContext(), R.layout.layout_fragment_locationpreferences, null);
        dialog.setContentView(v);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) v.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            mBottomSheetBehavior = (BottomSheetBehavior) behavior;
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {


                    if (newState == 5)
                        getDialog().cancel();

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    v.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    int height = v.getMeasuredHeight();
                    mBottomSheetBehavior.setPeekHeight(height);
                }
            });
        }


        mRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        saveLocation = (Button) v.findViewById(R.id.savelocation);
        locationSearch = (EditText) v.findViewById(R.id.searchlocation);
        locationSearch.setVisibility(View.GONE);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        saveLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String data = "";
                getDialog().cancel();
                List<LocationModel> stList = ((LocationAdapter) mAdapter)
                        .getStudentist();

                List<LocationModel> temp = new ArrayList<LocationModel>();
                for (int i = 0; i < stList.size(); i++) {
                    if (stList.get(i).isSelected()) {
                        temp.add(stList.get(i));
                    }
                }


                ((SetPreferencesActivity) getActivity()).setLocations(temp);
            }
        });
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        getData();
                                    }
                                }
        );


        locationSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 0) {

                    mRecyclerView.setHasFixedSize(true);

                    // use a linear layout manager
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                    // create an Object for Adapter
                    mAdapter = new LocationAdapter(locationModels);

                    // set the adapter object to the Recyclerview
                    mRecyclerView.setAdapter(mAdapter);

                } else {


                    List<LocationModel> temp = new ArrayList<LocationModel>();
                    for (int j = 0; j < locationModels.size(); j++) {
                        if (locationModels.get(j).getCity().matches("(?i:.*" + locationSearch.getText().toString() + ".*)") || locationModels.get(j).getState().matches("(?i:.*" + locationSearch.getText().toString() + ".*)")) {
                            temp.add(locationModels.get(j));
                        }

                    }


                    mRecyclerView.setHasFixedSize(true);

                    // use a linear layout manager
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                    // create an Object for Adapter
                    mAdapter = new LocationAdapter(temp);

                    // set the adapter object to the Recyclerview
                    mRecyclerView.setAdapter(mAdapter);



                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    @Override
    public void onRefresh() {

        getData();
    }

    public void getData() {
       /* Call cm = new RequestApi().getInterface().getLocationPreference();

        cm.enqueue(new Callback<LocationModel>() {
            @Override
            public void onResponse(Response<LocationModel> response) {


                locationModels = response.body().getLocations();

                if (locationModels != null && locationModels.size() > 0) {

                    locationSearch.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    saveLocation.setVisibility(View.VISIBLE);
                    // use this setting to improve performance if you know that changes
                    // in content do not change the layout size of the RecyclerView
                    mRecyclerView.setHasFixedSize(true);

                    // use a linear layout manager
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                    // create an Object for Adapter
                    mAdapter = new LocationAdapter(locationModels);

                    // set the adapter object to the Recyclerview
                    mRecyclerView.setAdapter(mAdapter);
                } else {
                    locationSearch.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.GONE);
                    saveLocation.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }

            }

            @Override
            public void onFailure(Throwable t) {
                mRecyclerView.setVisibility(View.GONE);
                locationSearch.setVisibility(View.GONE);
                saveLocation.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }


        });*/


    }
}