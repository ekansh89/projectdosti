package com.tipstat.dosti.Fragments;

/**
 * Created by nikhi on 10/20/2016.
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.tipstat.dosti.Activities.SplashActivity;
import com.tipstat.dosti.Activities.UserProfileEdit;
import com.tipstat.dosti.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

/**
 * Created by MG on 17-07-2016.
 */
public class BottomSheetSelectionFragment extends BottomSheetDialogFragment {


    private static BottomSheetBehavior mBottomSheetBehavior;
    public List<String> data, languages;
    public String type;

    public static BottomSheetSelectionFragment newInstance(List<String> data, String type) {
        BottomSheetSelectionFragment f = new BottomSheetSelectionFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("data", (ArrayList<String>) data);
        args.putString("type", type);
        f.setArguments(args);

        return f;
    }

    public static BottomSheetSelectionFragment newInstance(List<String> data, String type, String[] languages) {
        BottomSheetSelectionFragment f = new BottomSheetSelectionFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("data", (ArrayList<String>) data);
        args.putString("type", type);

        ArrayList<String> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, languages);
        args.putStringArrayList("languages", arrayList);


        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = getArguments().getStringArrayList("data");
        languages = getArguments().getStringArrayList("languages");
        type = getArguments().getString("type");

    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View v = View.inflate(getContext(), R.layout.layout_fragment_bottomsheet_selectionfragment, null);
        dialog.setContentView(v);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) v.getParent()).getLayoutParams();
        final Button save = (Button) v.findViewById(R.id.save);
        final RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);


        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            mBottomSheetBehavior = (BottomSheetBehavior) behavior;
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {


                    if (newState == 5)
                        getDialog().cancel();

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            if (type.equalsIgnoreCase("language")) {
                v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        v.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        int height = v.getMeasuredHeight();
                        mBottomSheetBehavior.setPeekHeight(height);
                        save.setVisibility(View.VISIBLE);

                    }
                });
            } else {
                save.setVisibility(View.GONE);
            }
        }


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedItems = "";
                for (int i = 0; i < data.size(); i++) {
                    AppCompatCheckedTextView tv = (AppCompatCheckedTextView) mRecyclerView.getChildAt(i);
                    if (tv.isChecked()) {

                        selectedItems = selectedItems + tv.getText() + ",";

                    }


                }

                if (selectedItems != null && selectedItems.length() > 0 && selectedItems.charAt(selectedItems.length()-1)==',') {
                    selectedItems = selectedItems.substring(0, selectedItems.length()-1);
                }
                ((UserProfileEdit) getActivity()).setText("language", selectedItems);
                getDialog().cancel();
            }
        });

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(new RecyclerView.Adapter<PlanetViewHolder>() {

            @Override
            public PlanetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                if (!type.equalsIgnoreCase("language")) {
                    View v = LayoutInflater.from(parent.getContext()).inflate(
                            android.R.layout.simple_list_item_1,
                            parent,
                            false);
                    PlanetViewHolder vh = new PlanetViewHolder(v);
                    return vh;
                } else {
                    View v = LayoutInflater.from(parent.getContext()).inflate(
                            android.R.layout.simple_list_item_multiple_choice,
                            parent,
                            false);
                    PlanetViewHolder vh = new PlanetViewHolder(v);
                    return vh;
                }
            }

            @Override
            public void onBindViewHolder(PlanetViewHolder vh, int position) {
                if (!type.equalsIgnoreCase("language")) {
                    TextView tv = (TextView) vh.itemView;
                    tv.setText(data.get(position));
                } else {
                    AppCompatCheckedTextView tv = (AppCompatCheckedTextView) vh.itemView;
                    tv.setText(data.get(position));
                    if (languages != null && languages.size() > 0 && languages.contains(data.get(position)))
                        tv.setChecked(true);
                    else
                        tv.setChecked(false);
                }

            }

            @Override
            public int getItemCount() {
                return data.size();
            }
        });


    }


    private class PlanetViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        public PlanetViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (!type.equalsIgnoreCase("language")) {
                getDialog().cancel();
                if (type.equalsIgnoreCase("religion")) {
                    ((UserProfileEdit) getActivity()).setText("religion", ((TextView) v).getText().toString());

                } else if (type.equalsIgnoreCase("relationship")) {
                    ((UserProfileEdit) getActivity()).setText("relationship", ((TextView) v).getText().toString());

                } else if (type.equalsIgnoreCase("language")) {

                    ((UserProfileEdit) getActivity()).setText("language", ((TextView) v).getText().toString());
                }

            } else {
                AppCompatCheckedTextView checkedTextView = ((AppCompatCheckedTextView) v);


                if (checkedTextView.isChecked()) {
                    checkedTextView.setChecked(false);
                } else {
                    checkedTextView.setChecked(true);
                }


            }
        }
    }


}