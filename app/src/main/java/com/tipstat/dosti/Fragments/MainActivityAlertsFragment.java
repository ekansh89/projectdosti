package com.tipstat.dosti.Fragments;

/**
 * Created by nikhi on 10/18/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.google.gson.Gson;
import com.tipstat.dosti.Adapters.MainActivityAlertFragmentAdapter;
import com.tipstat.dosti.Models.MainActivityAlertFragmentModel;
import com.tipstat.dosti.Models.MainActivityAlertFragmentModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.ExpressInterestRequest;
import com.tipstat.dosti.RequestModels.FetchNotificationsRequest;
import com.tipstat.dosti.RequestModels.SetPreferncesRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Utils.Config;
import com.tipstat.dosti.Views.ErrorView.ErrorView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


public class MainActivityAlertsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;
    private MainActivityAlertFragmentAdapter mAdapter;
    private List<MainActivityAlertFragmentModel> list;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ErrorView errorView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_fragment_mainactivityalertsfragment, null);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        list = new ArrayList<>();
        mAdapter = new MainActivityAlertFragmentAdapter(list, getActivity());
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        errorView = (ErrorView) view.findViewById(R.id.errorview);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        prepareAlbums();
        //getData();

        swipeRefreshLayout.setOnRefreshListener(this);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        getData();
                                    }
                                }
        );

        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                if (recyclerView != null)
                    recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(true);
                getData();


            }
        });

        return view;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("activity result", "fromalert");

        if (resultCode == 2) {
          /*  String id = data.getStringExtra("id");
            String flag = data.getStringExtra("flag");
            MainActivityAlertFragmentModel mainActivityAlertFragmentModel = new MainActivityAlertFragmentModel();
            mainActivityAlertFragmentModel.setFb_id2(id);
            mainActivityAlertFragmentModel.setName(data.getStringExtra("name"));
            mainActivityAlertFragmentModel.setAge(data.getStringExtra("Age"));
            mainActivityAlertFragmentModel.setGender(data.getStringExtra("gender"));
            mainActivityAlertFragmentModel.setDatetime(data.getStringExtra("time"));
            mainActivityAlertFragmentModel.setProfilepic_url(data.getStringExtra("profilepic"));
            mainActivityAlertFragmentModel.setFlag(flag);
            list.add(mainActivityAlertFragmentModel);
            mAdapter.notifyDataSetChanged();

            if (list != null && list.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
            }
*/
            list.clear();
            mAdapter.notifyDataSetChanged();
            getData();

        } else if (resultCode == 3) {
            String id = data.getStringExtra("id");
            String flag = data.getStringExtra("flag");
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getFb_id2().equals(id)) {
                    list.get(i).setFlag(flag);
                }

            }

            mAdapter.notifyDataSetChanged();
            if (list != null && list.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
            }

        }
    }

    public void getData() {

        try {

            if (getActivity() != null && isAdded()) {
                SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
                String userpreferences = pref.getString("userpreferences", null);
                String userCountry = pref.getString("usercountry", null);
                if (AccessToken.getCurrentAccessToken() != null) {

                    if (AccessToken.getCurrentAccessToken().getUserId() != null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {


                        Map<String, String> params = new HashMap<String, String>();
                        params.put("fb_id", AccessToken.getCurrentAccessToken().getUserId() + "");
                        if (userCountry != null && !userCountry.isEmpty() && (userCountry.equalsIgnoreCase("india") || userCountry.equalsIgnoreCase("pakistan") || userCountry.equalsIgnoreCase("pakisthan"))) {
                            params.put("country", userCountry);
                        } else {
                            Gson gson = new Gson();
                            SetPreferncesRequest setPreferncesRequests = gson.fromJson(userpreferences, SetPreferncesRequest.class);
                            if (setPreferncesRequests != null && setPreferncesRequests.getCountry() != null && !setPreferncesRequests.getCountry().isEmpty()) {
                                if (setPreferncesRequests.getCountry().equalsIgnoreCase("india"))
                                    params.put("country", "pakistan");
                                else params.put("country", "india");
                            } else {
                                params.put("country", "");
                            }
                        }


                        Call<MainActivityAlertFragmentModel> cm = new RequestApi().getInterface().fetchNotifications(params);

                        cm.enqueue(new Callback<MainActivityAlertFragmentModel>() {
                            @Override
                            public void onResponse(Response<MainActivityAlertFragmentModel> response) {

                                try {

                                    if (isAdded()) {
                                        if (response != null && response.body() != null && response.body().getUsers() != null && response.body().getUsers().size() > 0) {

                                            if(list!=null && list.size()>0)
                                            {
                                                list.clear();
                                                mAdapter.notifyDataSetChanged();
                                            }
                                            swipeRefreshLayout.setRefreshing(false);
                                            list.addAll(response.body().getUsers());
                                            recyclerView.setVisibility(View.VISIBLE);
                                            errorView.setVisibility(View.GONE);
                                            mAdapter.notifyDataSetChanged();
                                        } else {
                                            swipeRefreshLayout.setRefreshing(false);
                                            recyclerView.setVisibility(View.GONE);
                                            errorView.setVisibility(View.VISIBLE);
                                            errorView.setSubtitle("No Alerts right now");
                                            errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                                            errorView.setImage(getResources().getDrawable(R.mipmap.ic_sad));
                                        }
                                    }
                                    else
                                    {
                                        Log.d("fragment","no activty");
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(Throwable t) {

                                try {
                                    if (isAdded()) {
                                        swipeRefreshLayout.setRefreshing(false);
                                        recyclerView.setVisibility(View.GONE);
                                        errorView.setVisibility(View.VISIBLE);
                                        errorView.setSubtitle("Unable to connect server");
                                        errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                                        errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));


                                    }
                                    else
                                    {
                                        Log.d("fragment","no activty");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }


                        });
                    } else {
                        swipeRefreshLayout.setRefreshing(false);
                        recyclerView.setVisibility(View.GONE);
                        errorView.setVisibility(View.VISIBLE);
                        errorView.setSubtitle("Unable to connect server");
                        errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                        errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));
                    }
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.setVisibility(View.GONE);
                    errorView.setVisibility(View.VISIBLE);
                    errorView.setSubtitle("Unable to connect server");
                    errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                    errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));
                }

            }
            else
            {
                Log.d("fragment","no activty");
            }

        } catch (Exception e) {

        }
    }

    private void prepareAlbums() {


    }

    @Override
    public void onRefresh() {
        list.clear();
        mAdapter.notifyDataSetChanged();
        getData();
    }


    public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public SimpleDividerItemDecoration(Context context) {
            mDivider = context.getResources().getDrawable(R.drawable.recyclerview_line_divider);
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }


}