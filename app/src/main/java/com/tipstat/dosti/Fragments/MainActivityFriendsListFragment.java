package com.tipstat.dosti.Fragments;

/**
 * Created by nikhi on 10/18/2016.
 */

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.tipstat.dosti.Adapters.MainActivityFriendsListFragmentAdapter;
import com.tipstat.dosti.Models.MainActivityFriendsListModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.MutualFriendsRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Views.ErrorView.ErrorView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class MainActivityFriendsListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;
    private MainActivityFriendsListFragmentAdapter mAdapter;
    private List<MainActivityFriendsListModel> list;
    private EditText countedittext;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ErrorView errorView;
    private boolean countClickBoolean = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_fragment_friendslist, null);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        list = new ArrayList<>();
        mAdapter = new MainActivityFriendsListFragmentAdapter(list, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        countedittext = (EditText) view.findViewById(R.id.allfriendstitle);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        errorView = (ErrorView) view.findViewById(R.id.errorview);
        swipeRefreshLayout.setOnRefreshListener(this);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        getData();
                                    }
                                }
        );

        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                if (recyclerView != null)
                    recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(true);
                getData();
            }
        });

        countedittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!countClickBoolean) {
                    countClickBoolean = true;


                    countedittext.setCursorVisible(true);
                    countedittext.setText("");
                    countedittext.setHint("Search");

                    Drawable image = getResources().getDrawable(R.mipmap.ic_clear_black_24dp);
                    int h = image.getIntrinsicHeight();
                    int w = image.getIntrinsicWidth();
                    image.setBounds(0, 0, w, h);
                    countedittext.setCompoundDrawables(null, null, image, null);

                    countedittext.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            final int DRAWABLE_LEFT = 0;
                            final int DRAWABLE_TOP = 1;
                            final int DRAWABLE_RIGHT = 2;
                            final int DRAWABLE_BOTTOM = 3;

                            if (event.getAction() == MotionEvent.ACTION_UP) {
                                if (event.getRawX() >= (countedittext.getRight() - countedittext.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                    // your action here


                                    if (countedittext.getText().length() == 0) {
                                        countClickBoolean = false;
                                        countedittext.setCursorVisible(false);

                                        SpannableStringBuilder builder = new SpannableStringBuilder();
                                        SpannableString str1 = new SpannableString("All Friends: ");
                                        str1.setSpan(new ForegroundColorSpan(Color.GRAY), 0, str1.length(), 0);
                                        builder.append(str1);
                                        SpannableString str2 = new SpannableString(list.size() + "");
                                        str2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, str2.length(), 0);
                                        builder.append(str2);
                                        countedittext.setText(builder, TextView.BufferType.SPANNABLE);

                                        Drawable image = getResources().getDrawable(R.mipmap.ic_search_black_24dp);
                                        int h = image.getIntrinsicHeight();
                                        int w = image.getIntrinsicWidth();
                                        image.setBounds(0, 0, w, h);
                                        countedittext.setCompoundDrawables(null, null, image, null);


                                        mAdapter = new MainActivityFriendsListFragmentAdapter(list, getActivity());
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                        recyclerView.setLayoutManager(mLayoutManager);
                                        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
                                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                                        recyclerView.setAdapter(mAdapter);


                                        View view = getActivity().getCurrentFocus();
                                        if (view != null) {
                                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                        }


                                    } else {
                                        countedittext.getText().clear();

                                        Drawable image = getResources().getDrawable(R.mipmap.ic_clear_black_24dp);
                                        int h = image.getIntrinsicHeight();
                                        int w = image.getIntrinsicWidth();
                                        image.setBounds(0, 0, w, h);
                                        countedittext.setCompoundDrawables(null, null, image, null);


                                        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                                        inputMethodManager.toggleSoftInputFromWindow(countedittext.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
                                        countedittext.requestFocus();
                                    }


                                    return true;
                                }
                            }
                            return false;
                        }
                    });


                    countedittext.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                            if (s.length() == 0) {
                                mAdapter = new MainActivityFriendsListFragmentAdapter(list, getActivity());
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(mAdapter);
                            } else {

                                if (!s.toString().contains("All Friends")) {
                                    List<MainActivityFriendsListModel> temp = new ArrayList<MainActivityFriendsListModel>();
                                    for (int j = 0; j < list.size(); j++) {
                                        if (list.get(j).getName().matches("(?i:.*" + countedittext.getText().toString() + ".*)")) {
                                            temp.add(list.get(j));
                                        }

                                    }
                                    mAdapter = new MainActivityFriendsListFragmentAdapter(temp, getActivity());
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                    recyclerView.setLayoutManager(mLayoutManager);
                                    recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    recyclerView.setAdapter(mAdapter);
                                }
                            }

                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                }


            }
        });

        return view;

    }

    @Override
    public void onRefresh() {
        list.clear();
        mAdapter.notifyDataSetChanged();
        getData();
    }

    public void getData() {

        try {

            if (isAdded()) {
                if (AccessToken.getCurrentAccessToken() != null) {

                    if (AccessToken.getCurrentAccessToken().getUserId() != null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {

                        MutualFriendsRequest mutualFriendsRequest = new MutualFriendsRequest();
                        mutualFriendsRequest.setFb_id(AccessToken.getCurrentAccessToken().getUserId());

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("fb_id", AccessToken.getCurrentAccessToken().getUserId() + "");

                        Call<MainActivityFriendsListModel> cm = new RequestApi().getInterface().mutualFriends(params);

                        cm.enqueue(new Callback<MainActivityFriendsListModel>() {
                            @Override
                            public void onResponse(retrofit.Response<MainActivityFriendsListModel> response) {
                                if (isAdded()) {
                                    if (response != null && response.body() != null && response.body().getUsers() != null && response.body().getUsers().size() > 0) {

                                        if(list!=null && list.size()>0)
                                        {
                                            list.clear();
                                            mAdapter.notifyDataSetChanged();
                                        }

                                        prepareAlbums(response.body().getUsers());
                                        SpannableStringBuilder builder = new SpannableStringBuilder();
                                        SpannableString str1 = new SpannableString("All Friends: ");
                                        str1.setSpan(new ForegroundColorSpan(Color.GRAY), 0, str1.length(), 0);
                                        builder.append(str1);
                                        SpannableString str2 = new SpannableString(response.body().getUsers().size() + "");
                                        str2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, str2.length(), 0);
                                        builder.append(str2);
                                        countedittext.setText(builder, TextView.BufferType.SPANNABLE);
                                        countedittext.setVisibility(View.VISIBLE);
                                        swipeRefreshLayout.setRefreshing(false);
                                        recyclerView.setVisibility(View.VISIBLE);

                                    } else {

                                        countedittext.setVisibility(View.GONE);
                                        swipeRefreshLayout.setRefreshing(false);
                                        recyclerView.setVisibility(View.GONE);
                                        errorView.setVisibility(View.VISIBLE);
                                        errorView.setSubtitle("No friends till now");
                                        errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                                        errorView.setImage(getResources().getDrawable(R.mipmap.ic_sad));


                                    }
                                } else {
                                    Log.d("fragment", "no activty");
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                if (isAdded()) {
                                    swipeRefreshLayout.setRefreshing(false);

                                    recyclerView.setVisibility(View.GONE);
                                    countedittext.setVisibility(View.GONE);
                                    errorView.setVisibility(View.VISIBLE);
                                    errorView.setSubtitle("Unable to connect server");
                                    errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                                    errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));
                                } else {
                                    Log.d("fragment", "no activty");
                                }
                            }

                        });
                    } else {
                        swipeRefreshLayout.setRefreshing(false);
                        countedittext.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        errorView.setVisibility(View.VISIBLE);
                        errorView.setSubtitle("Unable to connect server");
                        errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                        errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));
                    }

                } else {

                    swipeRefreshLayout.setRefreshing(false);
                    countedittext.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    errorView.setVisibility(View.VISIBLE);
                    errorView.setSubtitle("Unable to connect server");
                    errorView.setRetryButtonTextColor(getResources().getColor(android.R.color.white));
                    errorView.setImage(getResources().getDrawable(R.mipmap.ic_server));
                }
            } else {
                Log.d("fragment", "no activty");
            }
        } catch (Exception e) {

        }
    }

    private void prepareAlbums(List<MainActivityFriendsListModel> data) {

        list.addAll(data);
        mAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        recyclerView.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
    }


    public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public SimpleDividerItemDecoration(Context context) {
            mDivider = context.getResources().getDrawable(R.drawable.recyclerview_line_divider);
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }


}