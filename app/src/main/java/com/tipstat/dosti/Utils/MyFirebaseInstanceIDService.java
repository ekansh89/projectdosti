package com.tipstat.dosti.Utils;

/**
 * Created by nikhi on 10/22/2016.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.squareup.picasso.Picasso;
import com.tipstat.dosti.Models.LoginModel;
import com.tipstat.dosti.R;
import com.tipstat.dosti.RequestModels.FcmIdRequest;
import com.tipstat.dosti.RequestModels.LoginApiRequest;
import com.tipstat.dosti.Retrofit.RequestApi;
import com.tipstat.dosti.Views.ChipView.Contact;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //TODO your background code


                if(AccessToken.getCurrentAccessToken()!=null) {

                   if(AccessToken.getCurrentAccessToken().getUserId()!=null && !AccessToken.getCurrentAccessToken().getUserId().isEmpty()) {
                       FcmIdRequest fcmIdRequest = new FcmIdRequest();
                       fcmIdRequest.setFb_id(AccessToken.getCurrentAccessToken().getUserId() + "");
                       fcmIdRequest.setFcm(token);

                       Call<FcmIdRequest> cm = new RequestApi().getInterface().fcmregistartion(fcmIdRequest);

                       cm.enqueue(new Callback<FcmIdRequest>() {
                           @Override
                           public void onResponse( Response<FcmIdRequest> response) {

                           }

                           @Override
                           public void onFailure(  Throwable t) {

                           }


                       });
                   }
                    else
                       Log.d("fcmfromservice","null string");
                }
                else
                    Log.d("fcmfromservice","null access token");

            }
        });

    }


    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
    }
}