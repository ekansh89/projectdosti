package com.tipstat.dosti.Utils;

/**
 * Created by nikhi on 10/22/2016.
 */

public class Config {


    // public static final String BaseUrl="http://35.160.32.223/";
    // public  static  final String BaseUrl="http://tipstatweb.com/";
    //public static final String BaseUrl = "http://192.168.0.167/";
    public static final String BaseUrl = "http://52.204.229.198/";
    public static final String BaseImageUrl = BaseUrl + "dosti/";

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
}